//
//  NightFishRechargePayWayTableViewCell.h
//  NightFish
//
//  Created by steven vicky on 2019/5/21.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NightFishRechargePayWayTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *payImageView;
@property (weak, nonatomic) IBOutlet UILabel *payLabel;
@property (weak, nonatomic) IBOutlet UIButton *payButton;

@end

NS_ASSUME_NONNULL_END
