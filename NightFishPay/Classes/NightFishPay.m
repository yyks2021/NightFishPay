//
//  NightFishPay.m
//  NightFish
//
//  Created by 魏琦 on 2019/3/5.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import "NightFishPay.h"
#import <WXApi.h>
#import <AlipaySDK/AlipaySDK.h>

@interface NightFishPay ()<WXApiDelegate>

@end
@implementation NightFishPay
+(instancetype)shareInstance {
    static NightFishPay *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[NightFishPay alloc] init];
    });
    
    return instance;
}

- (void)wxPayWithPayParam:(NSString *)pay_param successBlock:(paySuccessCallBack)successBlock failureBlock:(payFailureCallBack)failureBlock{
    //Json解析
    self.successBlock = successBlock;
    self.failureBlock = failureBlock;
    NSData * data = [pay_param dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    if(error != nil) {
        self.failureBlock(WXERROR_PARAM);
        return ;
    }
    //设置必要的参数
    NSString *appid = dic[@"appid"];
    NSString *partnerid = dic[@"partnerid"];
    NSString *prepayid = dic[@"prepayid"];
    NSString *package = dic[@"package"];
    NSString *noncestr = dic[@"noncestr"];
    NSString *timestamp = dic[@"timestamp"];
    NSString *sign = dic[@"sign"];
    
    [WXApi registerApp:appid];
    
    if(![WXApi isWXAppInstalled]) {
        self.failureBlock(WXERROR_NOTINSTALL);
       
        return ;
    }
    if (![WXApi isWXAppSupportApi]) {
        self.failureBlock(WXERROR_UNSUPPORT);
        
        return ;
    }
    
    //发起微信支付
    PayReq* req   = [[PayReq alloc] init];
    //微信分配的商户号
    req.partnerId = partnerid;
    //微信返回的支付交易会话ID
    req.prepayId  = prepayid;
    // 随机字符串，不长于32位
    req.nonceStr  = noncestr;
    // 时间戳
    req.timeStamp = timestamp.intValue;
    //暂填写固定值Sign=WXPay
    req.package   = package;
    //签名
    req.sign      = sign;
    [WXApi sendReq:req];
}

// 微信终端返回给第三方的关于支付结果的结构体
- (void)onResp:(BaseResp *)resp
{
    if ([resp isKindOfClass:[PayResp class]])
    {
        switch (resp.errCode) {
                case WXSuccess:
                self.successBlock(WXSUCESS);
                break;
                
                case WXErrCodeUserCancel:   //用户点击取消并返回
                self.failureBlock(WXSCANCEL);
                break;
                
            default:        //剩余都是支付失败
                self.failureBlock(WXERROR);
                break;
        }
    }
}

-(void)aliPayWithPayParam:(NSString *)pay_param successBlock:(paySuccessCallBack)successBlock failureBlock:(payFailureCallBack)failureBlock
{
    self.successBlock = successBlock;
    self.failureBlock = failureBlock;
    NSString * appScheme =  @"alipayNightFish";
    [[AlipaySDK defaultService] payOrder:pay_param fromScheme:appScheme callback:^(NSDictionary *resultDic) {
        NSLog(@"----- %@",resultDic);
        NSInteger resultCode = [resultDic[@"resultStatus"] integerValue];
        switch (resultCode) {
                case 9000:     //支付成功
                self.successBlock(ALIPAYSUCESS);
                break;
                case 6001:     //支付取消
                self.successBlock(ALIPAYCANCEL);
                break;
            default:        //支付失败
                self.successBlock(ALIPAYCANCEL);
                break;
        }
    }];
}

//url 编码
- (NSString*)urlEncodedString:(NSString *)string
{
    NSString * encodedString = [string stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    return encodedString;
}

///回调处理
- (BOOL) handleOpenURL:(NSURL *) url
{
    if ([url.host isEqualToString:@"safepay"])
    {
        // 支付跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            //【由于在跳转支付宝客户端支付的过程中，商户app在后台很可能被系统kill了，所以pay接口的callback就会失效，请商户对standbyCallback返回的回调结果进行处理,就是在这个方法里面处理跟callback一样的逻辑】
            NSLog(@"result = %@",resultDic);
            
            NSInteger resultCode = [resultDic[@"resultStatus"] integerValue];
            switch (resultCode) {
                    case 9000:     //支付成功
                    self.successBlock(ALIPAYSUCESS);
                    break;
                    
                    case 6001:     //支付取消
                    self.failureBlock(ALIPAYCANCEL);
                    break;
                    
                default:        //支付失败
                    self.failureBlock(ALIPAYERROR);
                    break;
            }
        }];
        
        // 授权跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processAuth_V2Result:url standbyCallback:^(NSDictionary *resultDic) {
            NSLog(@"result = %@",resultDic);
            // 解析 auth code
            NSString *result = resultDic[@"result"];
            NSString *authCode = nil;
            if (result.length>0) {
                NSArray *resultArr = [result componentsSeparatedByString:@"&"];
                for (NSString *subResult in resultArr) {
                    if (subResult.length > 10 && [subResult hasPrefix:@"auth_code="]) {
                        authCode = [subResult substringFromIndex:10];
                        break;
                    }
                }
            }
            NSLog(@"授权结果 authCode = %@", authCode?:@"");
        }];
        return YES;
    } //([url.host isEqualToString:@"pay"]) //微信支付
    return [WXApi handleOpenURL:url delegate:self];
}


@end
