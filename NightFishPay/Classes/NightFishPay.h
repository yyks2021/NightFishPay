//
//  NightFishPay.h
//  NightFish
//
//  Created by 魏琦 on 2019/3/5.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef NS_ENUM(NSInteger, PayCode) {
    WXSUCESS            = 1001,   /**< 成功    */
    WXERROR             = 1002,   /**< 失败    */
    WXSCANCEL           = 1003,   /**< 取消    */
    WXERROR_NOTINSTALL  = 1004,   /**< 未安装微信   */
    WXERROR_UNSUPPORT   = 1005,   /**< 微信不支持    */
    WXERROR_PARAM       = 1006,   /**< 支付参数解析错误   */
    
    ALIPAYSUCESS        = 1101,   /**< 支付宝支付成功 */
    ALIPAYERROR         = 1102,   /**< 支付宝支付错误 */
    ALIPAYCANCEL        = 1103,   /**< 支付宝支付取消 */
};
NS_ASSUME_NONNULL_BEGIN

typedef  void(^paySuccessCallBack)(PayCode code);
typedef  void(^payFailureCallBack)(PayCode code);

@interface NightFishPay : NSObject
@property (nonatomic, copy) payFailureCallBack failureBlock;
@property (nonatomic, copy) payFailureCallBack successBlock;

+ (instancetype)shareInstance;
-(void)aliPayWithPayParam:(NSString *)pay_param successBlock:(paySuccessCallBack)successBlock failureBlock:(payFailureCallBack)failureBlock;
- (void)wxPayWithPayParam:(NSString *)pay_param successBlock:(paySuccessCallBack)successBlock failureBlock:(payFailureCallBack)failureBlock;
- (BOOL) handleOpenURL:(NSURL *)url;
@end

NS_ASSUME_NONNULL_END
