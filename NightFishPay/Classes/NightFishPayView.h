//
//  NightFishPayView.h
//  NightFish
//
//  Created by 魏琦 on 2019/3/7.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,NightFishPayWay){
    NightFishWXPay = 0, //"wxpay"
    NightFishALPay =1 //"alipay"
};

NS_ASSUME_NONNULL_BEGIN

@interface NightFishPayView : UIView
@property (nonatomic,copy)void(^payWayBlcok)(NightFishPayWay);
@property (nonatomic, strong) NSIndexPath *selectPath;
@end

NS_ASSUME_NONNULL_END
