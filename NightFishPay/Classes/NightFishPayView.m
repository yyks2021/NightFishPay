//
//  NightFishPayView.m
//  NightFish
//
//  Created by 魏琦 on 2019/3/7.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import "NightFishPayView.h"
#import <GKCover/GKCover.h>
#import "NightFishPayTableViewCell.h"
#import "NightFishPayFooterView.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
@interface NightFishPayView ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (nonatomic, strong) NSArray *payList;
@property (weak, nonatomic) IBOutlet UIView *mainView;


@end

@implementation NightFishPayView

-(void)awakeFromNib {
    [super awakeFromNib];
    _selectPath = [NSIndexPath indexPathForRow:1 inSection:0];
    _tableview.delegate = self;
    _tableview.dataSource = self;
    NSString *filePath = [[self ma_AssistantBundle] pathForResource:@"pay_way_list" ofType:@"plist"];
    _payList = [NSArray arrayWithContentsOfFile:filePath];
    NSString *bundleName = @"NightFishPay.bundle";
    // xib名称需要拼接Bundle名称，否则找不到xib
    NSString *nibName = [NSString stringWithFormat:@"%@/NightFishPayFooterView",bundleName];
    NSString *nibName2 = [NSString stringWithFormat:@"%@/NightFishPayTableViewCell",bundleName];
    [_tableview registerNib:[UINib nibWithNibName:nibName2 bundle:[NSBundle bundleForClass:[NightFishPayView class]]] forCellReuseIdentifier:NSStringFromClass([NightFishPayTableViewCell class])];
    @weakify(self);
    [[_closeBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        [GKCover hideCover];
        
    }];
    _tableview.rowHeight = 55;
    
    _tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    NightFishPayFooterView *footerView = [[NSBundle bundleForClass:[NightFishPayView class]] loadNibNamed:nibName owner:nil options:nil].firstObject;
    [[footerView.sureButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self);
        [GKCover hideCover];
        
        if (self.payWayBlcok) {
            if (self.selectPath.item == 0) {
                self.payWayBlcok(NightFishWXPay);
            }else {
                self.payWayBlcok(NightFishALPay);
            }
        }
    }];
    _tableview.tableFooterView = footerView;
}

- (NSURL *)ma_AssistantLibraryBundleURL {
    NSBundle *bundle = [NSBundle bundleForClass:[NightFishPayView class]];
    return [bundle URLForResource:@"source" withExtension:@"bundle"];
}

- (NSBundle *) ma_AssistantBundle {
    //再获取我们自己手动创建的bundle
    return [NSBundle bundleWithURL:[self ma_AssistantLibraryBundleURL]];
}

- (UIImage *)ma_AssistantImageNamed:(NSString *)imageName
{
    NSInteger scale = [UIScreen mainScreen].scale;
    UIImage *loadingImage = [[UIImage imageWithContentsOfFile:[[self ma_AssistantBundle] pathForResource:[NSString stringWithFormat:@"%@@%zdx",imageName,scale] ofType:@"png"]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    return loadingImage;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.payList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NightFishPayTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([NightFishPayTableViewCell class]) forIndexPath:indexPath];
    NSDictionary *dic = self.payList[indexPath.item];
    cell.payImageView.image = [self ma_AssistantImageNamed:dic[@"image"]];
    cell.payLabel.text = dic[@"title"];
    NSLog(@"%@",indexPath);
    NSLog(@"%@",indexPath);
    if(_selectPath.section== indexPath.section)
    {
        if (_selectPath.item == indexPath.item) {
            [cell.payButton setImage:[self ma_AssistantImageNamed:@"pay_way_select"] forState:UIControlStateNormal];
        }
        else {
            [cell.payButton setImage:[self ma_AssistantImageNamed:@"pay_normal"] forState:UIControlStateNormal];
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    int newRow = (int)indexPath.row;
    int oldRow = (int)(_selectPath != nil) ? (int)_selectPath.row : -1;
    
    if(newRow !=  oldRow) { //selectedButton : 我这里是cell中得一个按钮属性
        NightFishPayTableViewCell *newCell = (NightFishPayTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        [newCell.payButton setImage:[self ma_AssistantImageNamed:@"pay_way_select"] forState:UIControlStateNormal];
        
        NightFishPayTableViewCell *oldCell = (NightFishPayTableViewCell *)[tableView cellForRowAtIndexPath:_selectPath]; //当_selectPath为-1时,返回cell == nil
        [oldCell.payButton setImage:[self ma_AssistantImageNamed:@"pay_normal"] forState:UIControlStateNormal];
        _selectPath= [indexPath copy];//一定要这么写，要不报错
    }
    
}




/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
