//
//  NightFishPayTableViewCell.m
//  NightFish
//
//  Created by 魏琦 on 2019/3/7.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import "NightFishPayTableViewCell.h"

@implementation NightFishPayTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _payLabel.font = [UIFont systemFontOfSize:16];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
