//
//  NightFishPayFooterView.h
//  NightFish
//
//  Created by 魏琦 on 2019/3/7.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NightFishPayFooterView : UIView
@property (weak, nonatomic) IBOutlet UIButton *sureButton;

@end

NS_ASSUME_NONNULL_END
