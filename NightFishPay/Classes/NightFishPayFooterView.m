//
//  NightFishPayFooterView.m
//  NightFish
//
//  Created by 魏琦 on 2019/3/7.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import "NightFishPayFooterView.h"
#import <UIColor+Hex.h>
#import <UIImage+Color.h>
#import <UIButton+regis.h>
#import <UIView+Layout.h>
#import <InlineFuncFile.h>
@interface  NightFishPayFooterView ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *payBottomView;


@end

@implementation NightFishPayFooterView


- (void)awakeFromNib {
    [super awakeFromNib];
    _sureButton.titleLabel.font = [UIFont systemFontOfSize:17];
    [_sureButton setBackgroundImage:js_imageWithOriginalImage([UIImage imageWithSolidColor:[UIColor colorWithHexString:@"2B3948"] size:_sureButton.size], 8) forState:UIControlStateNormal];
    if (navigationBarHeight() >= 80) {
        _payBottomView.constant = 33;
    }
    else {
        _payBottomView.constant = 15;
    }
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
