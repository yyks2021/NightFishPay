//
//  SceneDelegate.h
//  NightFishPay
//
//  Created by steven vicky on 2019/9/24.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

