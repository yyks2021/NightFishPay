//
//  NightFishHotelModel.h
//  NightFish
//
//  Created by 魏琦 on 2019/3/4.
//  Copyright © 2019 steven vicky. All rights reserved.
//
#import <UIKit/UIKit.h>
@class NightFishRoomInfo,NightFishHotelInfo,NightFishRoomsInfo,NightFishRoomCalendarInfo,NightFishSearchHotelModel,NightFishVipRoom,NightFishRoomDevice;
NS_ASSUME_NONNULL_BEGIN

@interface NightFishHotelModel : NSObject
@property (nonatomic, strong) NSArray <NightFishSearchHotelModel *>*list;
@property (nonatomic, copy) NSString *message;

@end

@interface NightFishSearchHotelModel:NSObject
@property (nonatomic, strong) NightFishHotelInfo *hotelInfoC2bHotelSearchDto;
@property (nonatomic, strong) NightFishHotelInfo *hotelInfo;
@property (nonatomic, copy) NSArray <NightFishRoomInfo *>* hotelInfoC2bHotelSearchRoomInfoVos;
@property (nonatomic, copy) NSArray <NightFishRoomInfo *>* hotelInfoHotelSearchHotelInfoRoomInfoVos;
@property (nonatomic, strong) NSArray <NightFishRoomCalendarInfo *> *roomCalendarInfos;
@property (nonatomic, strong) NSArray <NightFishRoomCalendarInfo *> *roomCalendarVipInfos;
@property (nonatomic, strong) NightFishRoomsInfo *roomInfo;
@property (nonatomic, copy) NSString *hotelId;
@property (nonatomic, assign) NSInteger distance;
@property (nonatomic,assign) double positionX;
@property (nonatomic,assign) double positionY;
@property (nonatomic,copy) NSString *hotelName;
@property (nonatomic,assign) NSInteger score;
@property (nonatomic,assign) long comfortLevel;
@property (nonatomic,copy) NSString *phone;
@property (nonatomic,strong) NSString *tel;
@property (nonatomic,assign) NSInteger starLevel;
@property (nonatomic, copy) NSString *star;
@property (nonatomic,copy) NSString *address;
@property (nonatomic,copy) NSArray <NightFishVipRoom *>*roomList;
@property (nonatomic,copy) NSString *imgUrl;
@property (nonatomic, assign) BOOL enabledCoupon;
@property (nonatomic, assign) NSInteger minPrice;
@property (nonatomic, assign) NSInteger count;
@property (nonatomic, copy) NSString *sourceId;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, strong)NSArray <NightFishRoomsInfo *>*rooms;
@property (nonatomic,assign) long identifier;
@property (nonatomic, copy) NSString *desc;
@end

@interface NightFishVipRoom:NSObject
@property (nonatomic, assign) long long salePrice;
@property (nonatomic, assign) NSInteger count;
@property (nonatomic, assign) long long roomId;
@property (nonatomic, strong) NightFishRoomInfo *roomInfo;
@end

@interface NightFishRoomInfo: NSObject

@property (nonatomic, strong) NightFishRoomsInfo *roomInfo;
@property (nonatomic, strong) NightFishRoomCalendarInfo * roomCalendarInfo;
@property (nonatomic,assign) long identifier;
@property (nonatomic,assign) long hotelId;
@property (nonatomic,copy) NSString *title;
@property (nonatomic,assign) BOOL valid;
@property (nonatomic,assign) long origPrice;
@property (nonatomic,assign) long bottomPrice;
@property (nonatomic,assign) long curPrice;
@property (nonatomic,copy) NSString *bedType;
@property (nonatomic,assign) long maxPeople;
@property (nonatomic,assign) long area;
@property (nonatomic,assign) long floor;
@property (nonatomic,copy) NSString *devices;
@property (nonatomic,assign) long available;
@property (nonatomic,assign) long count;
@property (nonatomic,assign) long isDel;
@property (nonatomic,assign) long long todayTime;
@property (nonatomic,copy) NSString *thumb;
@property (nonatomic,copy) NSString *desc;
@property (nonatomic,copy) NSString *extraInfo;
@property (nonatomic,copy) NSString *bedTypeCn;
@property (nonatomic,copy) NSString *devicesCn;
@property (nonatomic, strong) NSArray *picUrls;
@property (nonatomic, assign) NSInteger vipDiscountPrice;
@property (nonatomic, assign) NSInteger vipAvailable;
@property (nonatomic, assign) NSInteger vipFlag;
@property (nonatomic, assign) NSInteger vipCount;
@property (nonatomic, assign) NSInteger vipPrice;
@property (nonatomic, assign) NSInteger vipStatus;
@property (nonatomic,copy) NSString *vipExtra;
@property (nonatomic, assign) NSInteger salePrice;
@property (nonatomic, assign) NSInteger roomCount;
@property (nonatomic, copy) NSArray *images;
@property (nonatomic, assign) NSInteger minCount;
@property (nonatomic, assign) NSInteger minPrice;
@property (nonatomic, copy) NSString *sourceId;
@property (nonatomic, copy) NSString *sourceType;
@property (nonatomic, assign) NSInteger referencePrice;
@property (nonatomic, copy) NSString *roomInfoJson;
@property (nonatomic ,copy) NSString *devicesJson;
@property (nonatomic, copy) NSArray <NightFishRoomDevice *> *roomArray;
@property (nonatomic, copy) NSArray <NightFishRoomDevice *> *deviceArray;


@end

@interface NightFishHotelInfo : NSObject
@property (nonatomic,assign) NSInteger identifier;
@property (nonatomic,assign) NSInteger isDel;
@property (nonatomic,assign) long gmtCreate;
@property (nonatomic,assign) long gmtModified;
@property (nonatomic,assign) long operatorId;
@property (nonatomic,assign) long adminId;
@property (nonatomic,assign) long companyId;
@property (nonatomic,assign) long cityAgentId;
@property (nonatomic, assign) NSInteger distance;
@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSString *email;
@property (nonatomic,copy) NSString *contactName;
@property (nonatomic,copy) NSString *desc;
@property (nonatomic,copy) NSString *address;
@property (nonatomic,copy) NSString *principalInfo;
@property (nonatomic,copy) NSString *refundCfg;
@property (nonatomic,copy) NSString *phone;
@property (nonatomic,assign) long type;
@property (nonatomic,assign) long comfortLevel;
@property (nonatomic,assign) NSInteger starLevel;
@property (nonatomic,assign) NSInteger status;
@property (nonatomic,copy) NSString *extraInfo;
@property (nonatomic,assign) double positionX;
@property (nonatomic,assign) double positionY;
@property (nonatomic,assign) NSInteger score;
@property (nonatomic,assign) BOOL valid;
@property (nonatomic,assign) NSInteger parentTax;
@property (nonatomic,assign) NSInteger feePercent;
@property (nonatomic,strong) NSString *tel;
@property (nonatomic,assign) BOOL enabledCoupon;
@property (nonatomic, assign) BOOL isExpand;
@property (nonatomic, copy) NSString *star;
@property (nonatomic, assign) CGFloat descHeight;
@property (nonatomic, strong) NSMutableAttributedString *attrDesc;
@property (nonatomic, copy) NSArray *hotelPics;
@property (nonatomic, copy) NSArray *images;
@property (nonatomic, copy) NSArray <NightFishRoomInfo *>*rooms;
@property (nonatomic, assign) NSInteger minCount;
@property (nonatomic, assign) NSInteger minPrice;
@property (nonatomic, assign) CGFloat addressHeight;
@property (nonatomic, copy) NSString *sourceType;
@property (nonatomic, copy) NSString *sourceId;

@end

@interface NightFishRoomsInfo :NSObject

@property (nonatomic,assign) long identifier;
@property (nonatomic,assign) long hotelId;
@property (nonatomic,copy) NSString *title;
@property (nonatomic,assign) BOOL valid;
@property (nonatomic,assign) long origPrice;
@property (nonatomic,assign) long bottomPrice;
@property (nonatomic,assign) long curPrice;
@property (nonatomic,copy) NSString *bedType;
@property (nonatomic,assign) long maxPeople;
@property (nonatomic,assign) long area;
@property (nonatomic,assign) long floor;
@property (nonatomic,copy) NSString *devices;
@property (nonatomic,assign) long available;
@property (nonatomic,assign) long count;
@property (nonatomic,assign) long isDel;
@property (nonatomic,assign) long long todayTime;
@property (nonatomic,copy) NSString *thumb;
@property (nonatomic,copy) NSString *desc;
@property (nonatomic,copy) NSString *extraInfo;
@property (nonatomic,copy) NSString *bedTypeCn;
@property (nonatomic,copy) NSString *devicesCn;
@property (nonatomic, strong) NSArray *picUrls;
@property (nonatomic, assign) NSInteger vipDiscountPrice;
@property (nonatomic, assign) NSInteger vipAvailable;
@property (nonatomic, assign) NSInteger vipFlag;
@property (nonatomic, assign) NSInteger vipCount;
@property (nonatomic, assign) NSInteger vipPrice;
@property (nonatomic, assign) NSInteger vipStatus;
@property (nonatomic,copy) NSString *vipExtra;
@property (nonatomic, copy) NSArray *images;
@property (nonatomic, assign) NSInteger minCount;
@property (nonatomic, assign) NSInteger minPrice;
@property (nonatomic, copy) NSString *sourceId;
@property (nonatomic, assign) NSInteger referencePrice;

@property (nonatomic, copy) NSArray <NightFishRoomCalendarInfo *>*calendars;
@end


@interface NightFishRoomCalendarInfo : NSObject

@property (nonatomic,assign) long bottomPrice;
@property (nonatomic,assign) NSInteger count;
@property (nonatomic,assign) long long date;
@property (nonatomic,assign) long identifier;
@property (nonatomic,assign) long hotelId;
@property (nonatomic,assign) long lockCount;
@property (nonatomic,assign) NSInteger originPrice;
@property (nonatomic,assign) NSInteger roomId;
@property (nonatomic,assign) NSInteger salePrice;
@property (nonatomic,assign) long status;
@property (nonatomic, copy) NSString *sourceId;
@end


@interface NightFishHomeHotelSearchModel : NSObject
@property (nonatomic, strong) NSArray <NightFishHotelInfo *> *list;


@end


@interface NightFishRoomDevice : NSObject
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *icon;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *value;

@end
NS_ASSUME_NONNULL_END
