//
//  NFOrderDetailModel.h
//  NightFishMineOrderList
//
//  Created by 魏琦 on 2019/3/28.
//
//修改
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NFOrderDetailModel : NSObject
@property (nonatomic, assign) NSInteger area;
@property (nonatomic, assign) long long checkIn;
@property (nonatomic, assign) long long checkOut;
@property (nonatomic, assign) NSInteger count;
@property (nonatomic, assign) NSInteger days;
@property (nonatomic, copy) NSString *devices;
@property (nonatomic, copy) NSString *hotelAddress;
@property (nonatomic, assign) NSInteger hotelId;
@property (nonatomic, copy) NSString *hotelName;
@property (nonatomic, assign) NSInteger identifier;
@property (nonatomic, assign) NSInteger maxPeople;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *orderNo;
@property (nonatomic, assign) int origPrice;
@property (nonatomic ,copy) NSString *phone;
@property (nonatomic, assign) double positionX;
@property (nonatomic, assign) double positionY;
@property (nonatomic, assign) NSInteger roomId;
@property (nonatomic, copy) NSString *roomName;
@property (nonatomic, assign) NSInteger snapshotRefundFee;
@property (nonatomic, assign) NSInteger snapshotTotalFee;
@property (nonatomic, assign) NSInteger status;
@property (nonatomic, copy) NSString *tel;
@property (nonatomic, copy) NSString *bedType;
@property (nonatomic, assign) int curPrice;
@property (nonatomic, assign) int price;
@property (nonatomic, strong) NSArray *picUrls;
@property (nonatomic, assign) int totalPrice;
@property (nonatomic,copy) NSString *bedTypeCn;
@property(nonatomic,assign) NSInteger vipFlag; //0不是vip,1是vip
@property(nonatomic, copy) NSString *sourceType;
@property(nonatomic, assign) long sourceRoomId;
@property(nonatomic, assign) long sourceOrderId;
@property(nonatomic, assign) long sourceAmount;
@property(nonatomic, assign) long sourceHotelId;
@end

NS_ASSUME_NONNULL_END
