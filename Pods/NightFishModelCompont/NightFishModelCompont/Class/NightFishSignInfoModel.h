//
//  NightFishSignInfoModel.h
//  NightFish
//
//  Created by steven vicky on 2019/4/25.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NightFishSignInfoModel : NSObject
@property (nonatomic, assign) NSInteger continueDays;
@property (nonatomic, assign) BOOL isSign;
@property (nonatomic, assign) NSInteger remainAmount;
@property (nonatomic, assign) long long signTime;
@property (nonatomic, assign) NSInteger totalAmount;

@end

NS_ASSUME_NONNULL_END
