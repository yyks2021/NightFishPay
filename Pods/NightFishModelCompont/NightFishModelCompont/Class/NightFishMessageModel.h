//
//  NightFishMessageModel.h
//  NightFish
//
//  Created by steven vicky on 2019/4/24.
//  Copyright © 2019 steven vicky. All rights reserved.
//
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NightFishMessageModel : NSObject
@property (nonatomic, copy) NSString *content;
@property (nonatomic, assign) long long createdTime;
@property (nonatomic, assign) NSInteger hotelId;
@property (nonatomic, copy) NSString *iconUrl;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, assign) NSInteger identifier;
@property (nonatomic, assign) NSInteger type;
@property (nonatomic, assign) CGFloat height;
@property (nonatomic, copy) NSString *url;

@end

@interface NightFishMessage : NSObject

@property (nonatomic, strong) NSArray <NightFishMessageModel *>*list;
@property (nonatomic, assign) NSInteger count;

@end

NS_ASSUME_NONNULL_END
