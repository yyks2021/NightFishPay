//
//  NFOrderDetailModel.m
//  NightFishMineOrderList
//
//  Created by 魏琦 on 2019/3/28.
//

#import "NFOrderDetailModel.h"
#import <NSObject+YYModel.h>
@implementation NFOrderDetailModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"identifier" : @"id"};
    //从 json 过来的key 可以是id，ID，book_id。例子中 key 为 id。
}

@end
