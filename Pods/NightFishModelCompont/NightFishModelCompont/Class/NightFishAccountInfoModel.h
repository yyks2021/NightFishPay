//
//  NightFishAccountInfoModel.h
//  NightFish
//
//  Created by steven vicky on 2019/4/26.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NightFishAccountInfoModel : NSObject
@property (nonatomic, copy) NSString *inviteId;
@property (nonatomic, assign) NSInteger outAmount;
@property (nonatomic, assign) NSInteger peopleSize;
@property (nonatomic, assign) NSInteger remainAmount;
@property (nonatomic, assign) NSInteger totalInAmount;
@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *giftUrl;
@end

NS_ASSUME_NONNULL_END
