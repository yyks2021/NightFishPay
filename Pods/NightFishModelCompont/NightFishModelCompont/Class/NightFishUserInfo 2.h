//
//  NightFishUserInfo.h
//  NightFish
//
//  Created by steven vicky on 2019/2/28.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WCDB/WCDB.h>
NS_ASSUME_NONNULL_BEGIN

@interface NightFishUserInfo : NSObject <WCTTableCoding>
/*
 avatar = "<null>";
 id = 2;
 nickname = "User_6848399";
 password = "<null>";
 phone = 17682309322;
 status = 1;
 valid = 1;
 vipStatus = 1;
 
 */
@property (retain) NSString *avatar;
@property int identifier;
@property (retain) NSString *nickname;
@property (retain) NSString *password;
@property (retain) NSString *phone;
@property int status;
@property int valid;
@property int vipStatus;
@property (retain) NSString *token;
@property int loginStatus;
WCDB_PROPERTY(avatar);
WCDB_PROPERTY(token);
WCDB_PROPERTY(phone);
WCDB_PROPERTY(identifier);
WCDB_PROPERTY(nickname);
WCDB_PROPERTY(password);
WCDB_PROPERTY(status);
WCDB_PROPERTY(vipStatus);
WCDB_PROPERTY(valid);
WCDB_PROPERTY(loginStatus);
@end

NS_ASSUME_NONNULL_END
