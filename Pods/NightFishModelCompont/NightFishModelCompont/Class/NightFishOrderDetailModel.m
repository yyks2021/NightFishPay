//
//  NightFishOrderDetailModel.m
//  NightFishOrderPayCompont
//
//  Created by steven vicky on 2019/5/30.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import "NightFishOrderDetailModel.h"
#import <NSObject+YYModel.h>
#import <InlineFunction.h>
@implementation NightFishOrderDetailModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"identifier" : @"id"};
    //从 json 过来的key 可以是id，ID，book_id。例子中 key 为 id。
}

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"orderBills" : [NightFishOrderDetailOrderBills class],@"orderHead":[NightFishOrderDetailOrderHead class],@"hotelInfo":[NightFishOrderDetailHotelInfo class],@"roomInfo":[NightFishOrderDetailRoomInfo class]};
}
@end

@implementation NightFishOrderDetailRoomInfo
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"identifier" : @"id",@"desc":@"description"};
    //从 json 过来的key 可以是id，ID，book_id。例子中 key 为 id。
}

- (void)setRoomInfoJson:(NSString *)roomInfoJson {
    _roomInfoJson = roomInfoJson;
    
    NSArray *array = arrayWithJsonString(roomInfoJson);
    
    NSMutableArray *dataArray = @[].mutableCopy;
    if (array.count > 0) {
        for (NSDictionary *dic in array) {
            if (dic.count > 0) {
                NightFishOrderDetailRoomArray *device = [NightFishOrderDetailRoomArray yy_modelWithJSON:dic];
                [dataArray addObject:device];
            }
            
        }
    }
    
    _roomArray = dataArray.copy;
    
}


- (void)setDevicesJson:(NSString *)devicesJson {
    _devicesJson = devicesJson;
    NSArray *array = arrayWithJsonString(devicesJson);
    
    NSMutableArray *dataArray = @[].mutableCopy;
    if (array.count > 0) {
        for (NSDictionary *dic in array) {
            if (dic.count > 0) {
                NightFishOrderDetailRoomArray *device = [NightFishOrderDetailRoomArray yy_modelWithJSON:dic];
                [dataArray addObject:device];
            }
            
        }
    }
    
    _deviceArray = dataArray.copy;
}


@end

@implementation NightFishOrderDetailOrderHead
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"identifier" : @"id"};
    //从 json 过来的key 可以是id，ID，book_id。例子中 key 为 id。
}

@end

@implementation NightFishOrderDetailOrderBills

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"identifier" : @"id"};
    //从 json 过来的key 可以是id，ID，book_id。例子中 key 为 id。
}

@end

@implementation NightFishOrderDetailHotelInfo
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"identifier" : @"id",@"desc":@"description"};
    //从 json 过来的key 可以是id，ID，book_id。例子中 key 为 id。
}


@end


@implementation NightFishOrderDetailRoomArray



@end
