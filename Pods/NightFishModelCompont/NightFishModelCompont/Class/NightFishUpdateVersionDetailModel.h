//
//  NightFishUpdateVersionDetailModel.h
//  NightFish
//
//  Created by steven vicky on 2019/9/25.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NightFishUpdateVersionDetailModel : NSObject
@property(nonatomic, copy) NSString *img;
@property(nonatomic, copy) NSString *title;
@property(nonatomic, copy) NSArray *content;
@property(nonatomic, copy) NSString *url;
@end

NS_ASSUME_NONNULL_END
