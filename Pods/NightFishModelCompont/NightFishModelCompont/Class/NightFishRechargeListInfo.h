//
//  NightFishRechargeListInfo.h
//  NightFish
//
//  Created by steven vicky on 2019/5/23.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NightFishRechargeListInfo : NSObject
@property (nonatomic, copy) NSString *title;
@property (nonatomic, assign) NSInteger payAmount;
@property (nonatomic, assign) NSInteger giveAmount;
@property (nonatomic, assign) NSInteger userType;

@end

NS_ASSUME_NONNULL_END
