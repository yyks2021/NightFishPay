//
//  NightFishCardAdvantageInfo.h
//  NightFish
//
//  Created by steven vicky on 2019/5/23.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NightFishCardAdvantageInfo : NSObject
@property (nonatomic, copy) NSString *pic;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *picIcon;
@property (nonatomic, copy) NSString *url;
@end

NS_ASSUME_NONNULL_END
