//
//  NightFishUserMemberCardInfo.h
//  NightFish
//
//  Created by steven vicky on 2019/5/23.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NightFishUserMemberCardInfo : NSObject
@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, assign) NSInteger cardId;
@property (nonatomic, copy) NSString *cardNo;
@property (nonatomic, assign) NSInteger  hotelId;
@property (nonatomic, assign) NSInteger identifier;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, assign) NSInteger totalRemainAmount;
@property (nonatomic, assign) NSInteger userType;

@end

NS_ASSUME_NONNULL_END
