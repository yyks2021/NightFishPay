//
//  NightFishHelpModel.h
//  NightFish
//
//  Created by 魏琦 on 2019/3/13.
//  Copyright © 2019 steven vicky. All rights reserved.
//


#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN
@interface NightFishHelpModel : NSObject
@property (nonatomic, assign) long long gmt_create;
@property (nonatomic, assign) int parent;
@property (nonatomic, copy) NSString *cata;
@property (nonatomic, copy) NSString *caption;
@property (nonatomic, assign) int identifier;
@property (nonatomic, assign) long long gmt_modified;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, assign) int flag;
@property (nonatomic, assign) CGFloat height;
@end


@interface NightFishPersonCenterHelpModel : NSObject
@property (nonatomic, assign) int count;
@property (nonatomic, copy) NSArray <NightFishHelpModel *> *list;


@end

NS_ASSUME_NONNULL_END
