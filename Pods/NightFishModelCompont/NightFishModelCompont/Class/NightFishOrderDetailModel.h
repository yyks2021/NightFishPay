//
//  NightFishOrderDetailModel.h
//  NightFishOrderPayCompont
//
//  Created by steven vicky on 2019/5/30.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import <UIKit/UIKit.h>
@class NightFishOrderDetailRoomInfo,NightFishOrderDetailOrderHead,NightFishOrderDetailOrderBills,NightFishOrderDetailHotelInfo,
    NightFishOrderDetailRoomArray;
NS_ASSUME_NONNULL_BEGIN

@interface NightFishOrderDetailModel : NSObject
@property (nonatomic, strong) NightFishOrderDetailOrderHead *orderHead;
@property (nonatomic, strong) NSArray <NightFishOrderDetailOrderBills *>* orderBills;
@property (nonatomic, strong) NightFishOrderDetailHotelInfo *hotelInfo;
@property (nonatomic, strong) NightFishOrderDetailRoomInfo *roomInfo;

@end

@interface NightFishOrderDetailRoomInfo :NSObject
@property (nonatomic,assign) long identifier;
@property (nonatomic,assign) long hotelId;
@property (nonatomic,copy) NSString *title;
@property (nonatomic,assign) BOOL valid;
@property (nonatomic,assign) long origPrice;
@property (nonatomic,assign) long bottomPrice;
@property (nonatomic,assign) long curPrice;
@property (nonatomic,copy) NSString *bedType;
@property (nonatomic,assign) long maxPeople;
@property (nonatomic,assign) long area;
@property (nonatomic,assign) long floor;
@property (nonatomic,copy) NSString *devices;
@property (nonatomic,assign) long available;
@property (nonatomic,assign) long count;
@property (nonatomic,assign) long isDel;
@property (nonatomic,assign) long long todayTime;
@property (nonatomic,copy) NSString *thumb;
@property (nonatomic,copy) NSString *desc;
@property (nonatomic,copy) NSString *extraInfo;
@property (nonatomic,copy) NSString *bedTypeCn;
@property (nonatomic,copy) NSString *devicesCn;
@property (nonatomic, strong) NSArray *images;
@property (nonatomic, assign) NSInteger vipDiscountPrice;
@property (nonatomic, assign) NSInteger vipAvailable;
@property (nonatomic, assign) NSInteger vipFlag;
@property (nonatomic, assign) NSInteger vipCount;
@property (nonatomic, assign) NSInteger vipPrice;
@property (nonatomic, assign) NSInteger vipStatus;
@property (nonatomic,copy) NSString *vipExtra;
@property(nonatomic, assign) long sourceRoomId;
@property(nonatomic, assign) long sourceOrderId;
@property(nonatomic, assign) long sourceAmount;
@property(nonatomic, assign) long sourceHotelId;
@property(nonatomic, copy) NSString *sourceType;
@property (nonatomic, copy) NSString *roomInfoJson;
@property (nonatomic ,copy) NSString *devicesJson;
@property (nonatomic, copy) NSArray <NightFishOrderDetailRoomArray *> *roomArray;
@property (nonatomic, copy) NSArray <NightFishOrderDetailRoomArray *> *deviceArray;

@end

@interface NightFishOrderDetailOrderHead : NSObject
@property (nonatomic, assign) long identifier;
@property (nonatomic, assign) long long gmtCreate;
@property (nonatomic, assign) long long gmtModified;
@property (nonatomic, copy) NSString *orderNo;
@property (nonatomic, assign) long roomId;
@property (nonatomic, assign) long hotelId;
@property (nonatomic, strong) NSString *snapshotHotelName;
@property (nonatomic, strong) NSString *snapshotRoomName;
@property (nonatomic, assign) long long snapshotTotalFee;
@property (nonatomic, assign) long long snapshotRefundFee;
@property (nonatomic, strong) NSString *refundReason;
@property (nonatomic, assign) long long snapshotOriginTotalFee;
@property (nonatomic, assign) NSInteger status;
@property (nonatomic, assign) long long checkIn;
@property (nonatomic, assign) long long checkOut;
@property (nonatomic, assign) NSInteger days;
@property (nonatomic, assign) NSInteger roomCount;
@property (nonatomic, assign) NSInteger userCouponId;
@property (nonatomic, strong) NSString *contactName;
@property (nonatomic, strong) NSString *contactPhone;
@property (nonatomic, strong) NSString *paymentMethod;
@property (nonatomic, assign) NSInteger score;
@property (nonatomic, assign) NSInteger vipFlag;
@property(nonatomic, assign) long sourceRoomId;
@property(nonatomic, assign) long sourceOrderId;
@property(nonatomic, assign) long sourceAmount;
@property(nonatomic, assign) long sourceHotelId;
@property(nonatomic, copy) NSString *sourceType;
@end


@interface NightFishOrderDetailOrderBills : NSObject
@property (nonatomic, assign) long identifier;
@property (nonatomic, copy) NSString *orderNo;
@property (nonatomic, assign) long long inDate;
@property (nonatomic, assign) long originPrice;
@property (nonatomic, assign) long salePrice;
@property (nonatomic, assign) long dealPrice;
@property (nonatomic, assign) long bottomPrice;
@property (nonatomic, assign) NSInteger roomCount;
@property (nonatomic,copy) NSString *hotelExtraInfo;
@property (nonatomic,copy) NSString *roomExtraInfo;
@property (nonatomic,copy) NSString *roomCalendarInfo;
@property (nonatomic, assign) long long gmtCreate;
@property(nonatomic, assign) long sourceRoomId;
@property(nonatomic, assign) long sourceOrderId;
@property(nonatomic, assign) long sourceAmount;
@property(nonatomic, assign) long sourceHotelId;
@property(nonatomic, copy) NSString *sourceType;
@end

@interface NightFishOrderDetailHotelInfo : NSObject
@property (nonatomic,assign) NSInteger identifier;
@property (nonatomic,assign) NSInteger isDel;
@property (nonatomic,assign) long gmtCreate;
@property (nonatomic,assign) long gmtModified;
@property (nonatomic,assign) long operatorId;
@property (nonatomic,assign) long adminId;
@property (nonatomic,assign) long companyId;
@property (nonatomic,assign) long cityAgentId;
@property (nonatomic, assign) NSInteger distance;
@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSString *email;
@property (nonatomic,copy) NSString *contactName;
@property (nonatomic,copy) NSString *desc;
@property (nonatomic,copy) NSString *address;
@property (nonatomic,copy) NSString *principalInfo;
@property (nonatomic,copy) NSString *refundCfg;
@property (nonatomic,copy) NSString *phone;
@property (nonatomic,assign) long type;
@property (nonatomic,assign) long comfortLevel;
@property (nonatomic,assign) NSInteger starLevel;
@property (nonatomic,assign) NSInteger status;
@property (nonatomic,copy) NSString *extraInfo;
@property (nonatomic,assign) double positionX;
@property (nonatomic,assign) double positionY;
@property (nonatomic,assign) NSInteger score;
@property (nonatomic,assign) BOOL valid;
@property (nonatomic,assign) NSInteger parentTax;
@property (nonatomic,assign) NSInteger feePercent;
@property (nonatomic,strong) NSString *tel;
@property (nonatomic,assign) BOOL enabledCoupon;
@property (nonatomic, assign) BOOL isExpand;
@property (nonatomic, copy) NSString *star;
@property (nonatomic, assign) CGFloat descHeight;
@property (nonatomic, copy) NSArray *hotelPics;
@property(nonatomic, assign) long sourceRoomId;
@property(nonatomic, assign) long sourceOrderId;
@property(nonatomic, assign) long sourceAmount;
@property(nonatomic, assign) long sourceHotelId;
@property(nonatomic, copy) NSString *sourceType;
@end


@interface NightFishOrderDetailRoomArray : NSObject
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *icon;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *value;
@end
NS_ASSUME_NONNULL_END
