//
//  NightFishGoodListModel.h
//  NightFish
//
//  Created by steven vicky on 2019/4/25.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NightFishGoodListModel : NSObject
@property (nonatomic, copy) NSString *title;
@property (nonatomic, assign) NSInteger identifier;
@property (nonatomic, copy) NSString *image;
@property (nonatomic, assign) NSInteger needFishballCount;
@property (nonatomic, copy) NSString *desc;

@end

NS_ASSUME_NONNULL_END
