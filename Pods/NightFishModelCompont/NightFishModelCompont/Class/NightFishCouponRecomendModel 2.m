//
//  NightFishCouponRecomendModel.m
//  NightFish
//
//  Created by steven vicky on 2019/5/6.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import "NightFishCouponRecomendModel.h"
#import <YYModel.h>
@implementation NightFishCouponRecomendModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"identifier" : @"id"};
    //从 json 过来的key 可以是id，ID，book_id。例子中 key 为 id。
}

@end
