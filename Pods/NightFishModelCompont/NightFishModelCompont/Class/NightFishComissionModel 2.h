//
//  NightFishComissionModel.h
//  NightFish
//
//  Created by steven vicky on 2019/4/28.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NightFishComissionModel : NSObject
@property (nonatomic, assign) BOOL flag;
@property (nonatomic, assign) NSInteger remainAmount;
@property (nonatomic, assign) NSInteger status;
@property (nonatomic, assign) NSInteger overAmount;
@end

NS_ASSUME_NONNULL_END
