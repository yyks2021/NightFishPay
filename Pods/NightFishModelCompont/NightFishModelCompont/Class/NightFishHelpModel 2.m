//
//  NightFishHelpModel.m
//  NightFish
//
//  Created by 魏琦 on 2019/3/13.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import "NightFishHelpModel.h"
#import <YYModel.h>
#import <NFMacro.h>
@implementation NightFishHelpModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"identifier" : @"id"};
    //从 json 过来的key 可以是id，ID，book_id。例子中 key 为 id。
}

- (CGFloat)height {
    CGFloat height = [self.content boundingRectWithSize:CGSizeMake(AdapterSizeFromIphone6(345), CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} context:nil].size.height;
    return height;
}
@end

@implementation NightFishPersonCenterHelpModel
+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"list" : [NightFishHelpModel class] };
}


@end
