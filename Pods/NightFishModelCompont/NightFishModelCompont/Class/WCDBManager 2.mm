//
//  WCDBManager.m
//  NightFish
//
//  Created by steven vicky on 2019/2/28.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import "WCDBManager.h"
#import <WCDB/WCDB.h>
#import "NightFishUserInfo.h"
static WCDBManager *instance = nil;
static NSString *const UserTable = @"user";
@interface WCDBManager ()
@property (nonatomic,strong) WCTDatabase *dataBase;
@property (nonatomic,strong) WCTTable    *table;

@end
@implementation WCDBManager

+(WCDBManager *)defaultManager
{
    if (instance) {
        return instance;
    }
    @synchronized (self) {
        if (instance == nil) {
            instance = [[WCDBManager alloc]init];
            [instance creatDB];
        }
    }
    return instance;
}

- (BOOL)creatDB
{
    
    NSString *dbPath = [self getDBPath];
    NSLog(@"DBPath:%@",dbPath);
    self.dataBase = [[WCTDatabase alloc]initWithPath:dbPath];
    BOOL result   = [self.dataBase createTableAndIndexesOfName:UserTable withClass:NightFishUserInfo.class];
    self.table    = [self.dataBase getTableOfName:UserTable withClass:NightFishUserInfo.class];
    
    assert(result);
    
    
    if ([self.dataBase canOpen]) {
        NSLog(@"能打开数据库");
    }else{
        NSLog(@"不能打开数据库");
    }
    if ([self.dataBase isOpened]) {
        NSLog(@"打开中");
    }else{
        NSLog(@"没打开");
    }
    return result;
}

- (NSString *)getDBPath {
    NSString *databasePath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/database/userID"];
    databasePath = [databasePath stringByAppendingPathComponent:@"user.db"];
    return databasePath;
}

- (BOOL)insertUser:(NightFishUserInfo*)mod
{
    return [self.table insertObject:mod];
}

- (BOOL)updateUser:(NightFishUserInfo*)mod
{
    return [self.table updateRowsOnProperties:NightFishUserInfo.AllProperties withObject:mod where:NightFishUserInfo.identifier == mod.identifier];
}
- (NightFishUserInfo *)getUserWithId:(int)userID
{
    return [self.table getObjectsWhere:NightFishUserInfo.identifier == userID].firstObject;
}

- (BOOL)deleteUser:(NightFishUserInfo*)mod
{
    return [self.table deleteObjectsWhere:NightFishUserInfo.identifier == mod.identifier];
}

- (BOOL)deleteAllUsers
{
    return [self.table deleteAllObjects];
}

- (NSArray*)getAllUser
{
    return [self.table getAllObjects];
}

- (NSArray*) :(NSInteger)identifier
{
    return [self.table getObjectsWhere:NightFishUserInfo.identifier == identifier];
}

- (NightFishUserInfo*)getLoginUserWithLoginStatus:(int)longStatus {
    return [self.table getOneObjectWhere:NightFishUserInfo.loginStatus == longStatus];
}

- (NightFishUserInfo *)getLoginUserWithPhone:(NSString *)phone {
     return [self.table getOneObjectWhere:NightFishUserInfo.phone == phone];
}
@end
