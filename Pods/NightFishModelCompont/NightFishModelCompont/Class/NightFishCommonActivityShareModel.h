//
//  NightFishCommonActivityShareModel.h
//  NightFishWebViewCompont
//
//  Created by steven vicky on 2019/11/7.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface NightFishCommonActivityShareModel : NSObject
@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *img;
@property (nonatomic, copy) NSString *hdImage;
@property (nonatomic, assign) NSInteger state;
@property (nonatomic, strong) UIImage *hdImageData;
@property (nonatomic, strong) UIImage *thumImage;
@property (nonatomic, copy) NSString *path;
@end

NS_ASSUME_NONNULL_END
