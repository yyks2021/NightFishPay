//
//  NightFishCouponRecomendModel.h
//  NightFish
//
//  Created by steven vicky on 2019/5/6.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NightFishCouponRecomendModel : NSObject
@property (nonatomic, assign) NSInteger identifier;
@property (nonatomic, assign) long long  overTime;
@property (nonatomic, assign) NSInteger couponId;
@property (nonatomic, assign) NSInteger couponValue;
@property (nonatomic, copy) NSString *title;
@end

NS_ASSUME_NONNULL_END
