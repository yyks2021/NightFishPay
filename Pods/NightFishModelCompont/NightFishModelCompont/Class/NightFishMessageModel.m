//
//  NightFishMessageModel.m
//  NightFish
//
//  Created by steven vicky on 2019/4/24.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import "NightFishMessageModel.h"
#import <YYModel.h>
#import <NFMacro.h>
@implementation NightFishMessageModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"identifier" : @"id"};
    //从 json 过来的key 可以是id，ID，book_id。例子中 key 为 id。
}

- (CGFloat)height {
    UIFont * font = [UIFont systemFontOfSize:14];
    NSLog(@"%f",AdapterSizeFromIphone6(275));
    CGFloat height = [self.content boundingRectWithSize:CGSizeMake(AdapterSizeFromIphone6(275), CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin |NSStringDrawingUsesFontLeading |NSStringDrawingTruncatesLastVisibleLine attributes:@{NSFontAttributeName:font} context:nil].size.height;
    return height + 10;
}
@end


@implementation NightFishMessage
+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"list" : [NightFishMessageModel class] };
}



@end
