//
//  NightFishUpdateVersionModel.h
//  NightFish
//
//  Created by 魏琦 on 2019/4/3.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NightFishUpdateVersionModel : NSObject


@property (nonatomic, copy) NSString *version;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, assign) int status;
@property (nonatomic, copy) NSString *reason;
@property (nonatomic, copy) NSString *path;
@property (nonatomic, assign) int isUpdate;
@property (nonatomic, assign) int forcedUpdate;
@property (nonatomic, assign) long long createTime;
@property (nonatomic, copy) NSString *title;

@end

NS_ASSUME_NONNULL_END
