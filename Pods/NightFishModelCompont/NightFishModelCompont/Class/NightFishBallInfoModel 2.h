//
//  NightFishBallInfoModel.h
//  NightFish
//
//  Created by steven vicky on 2019/4/25.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NightFishBallInfoModel : NSObject
@property (nonatomic, assign) NSInteger avaliableAmount;
@property (nonatomic, assign) long long createdTime;
@property (nonatomic, assign) NSInteger operatorAmount;
@property (nonatomic, copy) NSString *remark;
@property (nonatomic, assign) NSInteger type;
@end

NS_ASSUME_NONNULL_END
