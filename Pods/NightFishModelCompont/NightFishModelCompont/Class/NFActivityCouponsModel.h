//
//  NFActivityCouponsModel.h
//  NightFishModelCompont
//
//  Created by steven vicky on 2019/12/9.
//  Copyright © 2019 魏琦. All rights reserved.
//

#import <Foundation/Foundation.h>
@class NFActivityCouponsList;
NS_ASSUME_NONNULL_BEGIN

@interface NFActivityCouponsModel : NSObject
@property (nonatomic, copy) NSString *title;
@property (nonatomic, strong) NSArray <NFActivityCouponsList *>*list;

@end


@interface NFActivityCouponsList : NSObject
@property (nonatomic, assign) long long overTime;
@property (nonatomic, assign) NSInteger price;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, assign) NSInteger type;
@property (nonatomic, assign) NSInteger validFee;



@end
NS_ASSUME_NONNULL_END
