//
//  NightFishComissionDetailModel.h
//  NightFish
//
//  Created by steven vicky on 2019/4/28.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface NightFishComissionDetailModel : NSObject
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *iconUrl;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, assign) long long createdTime;
@property (nonatomic, assign) CGFloat height;
@property (nonatomic, assign) NSInteger count;
@property (nonatomic, copy) NSString *giftUrl;
@property (nonatomic, assign) NSInteger price;
@property (nonatomic, copy) NSString *inviteCode;
@property (nonatomic, copy) NSString *url;
@end

NS_ASSUME_NONNULL_END
