//
//  NightFishUserModel.m
//  NightFish
//
//  Created by steven vicky on 2019/2/28.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import "NightFishUserModel.h"

@implementation NightFishUserModel
- (void)setToken:(NSString *)token {
    _token = token;
    _user_info.token = token;
}


@end
