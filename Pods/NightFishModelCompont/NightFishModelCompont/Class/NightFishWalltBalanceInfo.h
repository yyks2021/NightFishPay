//
//  NightFishWalltBalanceInfo.h
//  NightFish
//
//  Created by steven vicky on 2019/5/23.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface NightFishWalltBalanceInfo : NSObject
@property (nonatomic, copy) NSString *content;
@property (nonatomic, assign) long long createTime;
@property (nonatomic, copy) NSString *iconUrl;
@property (nonatomic, assign) NSInteger identifier;
@property (nonatomic, assign) CGFloat height;
@end

NS_ASSUME_NONNULL_END
