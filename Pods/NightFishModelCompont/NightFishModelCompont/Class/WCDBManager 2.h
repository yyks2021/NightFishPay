//
//  WCDBManager.h
//  NightFish
//
//  Created by steven vicky on 2019/2/28.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import <Foundation/Foundation.h>
@class NightFishUserInfo;
NS_ASSUME_NONNULL_BEGIN

@interface WCDBManager : NSObject

+(WCDBManager *)defaultManager;


- (BOOL)insertUser:(NightFishUserInfo*)mod;

- (BOOL)updateUser:(NightFishUserInfo*)mod;
- (NightFishUserInfo *)getUserWithId:(int)userID;
- (BOOL)deleteUser:(NightFishUserInfo*)mod;

- (BOOL)deleteAllUsers;

- (NSArray*)getAllUser;

- (NSArray*) :(NSInteger)identifier;

- (NightFishUserInfo *)getLoginUserWithLoginStatus:(int)longStatus;
- (NightFishUserInfo *)getLoginUserWithPhone:(NSString *)phone;
@end

NS_ASSUME_NONNULL_END
