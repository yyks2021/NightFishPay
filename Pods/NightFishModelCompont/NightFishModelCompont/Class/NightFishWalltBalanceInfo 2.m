//
//  NightFishWalltBalanceInfo.m
//  NightFish
//
//  Created by steven vicky on 2019/5/23.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import "NightFishWalltBalanceInfo.h"
#import <NFMacro.h>
@implementation NightFishWalltBalanceInfo
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"identifier" : @"id"};
    //从 json 过来的key 可以是id，ID，book_id。例子中 key 为 id。
}

- (CGFloat)height {
    CGFloat height = [self.content boundingRectWithSize:CGSizeMake(290, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} context:nil].size.height;
    return height;
}
@end
