//
//  NightFishGenPlanModel.h
//  NightFish
//
//  Created by steven vicky on 2019/9/25.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NightFishGenPlanModel : NSObject
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *tuijian;
@property (nonatomic, copy) NSString *chuangke;
@end

NS_ASSUME_NONNULL_END
