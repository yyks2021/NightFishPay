//
//  NightFishUserBindCardModel.h
//  NightFish
//
//  Created by steven vicky on 2019/4/29.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NightFishUserBindCardModel : NSObject
@property (nonatomic, copy) NSString *bankCard;
@property (nonatomic, copy) NSString *bankName;
@property (nonatomic, copy) NSString *userName;

@end

NS_ASSUME_NONNULL_END
