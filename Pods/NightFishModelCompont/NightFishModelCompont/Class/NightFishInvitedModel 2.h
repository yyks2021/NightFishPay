//
//  NightFishInvitedModel.h
//  NightFish
//
//  Created by steven vicky on 2019/9/26.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NightFishInvitedModel : NSObject
@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, assign) long long createTime;
@property (nonatomic, assign) NSInteger identifier;
@property (nonatomic, assign) NSInteger invitedT0Count;
@property (nonatomic, assign) NSInteger invitedT1Count;
@property (nonatomic, assign) NSInteger outAmount;
@property (nonatomic, assign) NSInteger remainAmount;
@property (nonatomic, assign) NSInteger totalInAmount;
@property (nonatomic, assign) NSInteger updateTime;
@property (nonatomic, assign) NSInteger userId;
@end

NS_ASSUME_NONNULL_END
