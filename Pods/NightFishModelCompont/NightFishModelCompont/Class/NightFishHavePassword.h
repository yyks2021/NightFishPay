//
//  NightFishHavePassword.h
//  NightFish
//
//  Created by steven vicky on 2019/2/28.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NightFishHavePassword : NSObject
@property (nonatomic, copy) NSString *password;
@end

NS_ASSUME_NONNULL_END
