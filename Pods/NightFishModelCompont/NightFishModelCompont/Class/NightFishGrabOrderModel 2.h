//
//  NightFishGrabOrderModel.h
//  NightFish
//
//  Created by steven vicky on 2019/8/16.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <NSObject+YYModel.h>
NS_ASSUME_NONNULL_BEGIN

@interface NightFishGrabOrderModel : NSObject

@property (nonatomic, copy) NSString *price;
@property (nonatomic, assign) NSInteger  comfortLevel;
@property (nonatomic, copy) NSString *positionX;
@property (nonatomic, copy) NSString *positionY;
@property (nonatomic, copy) NSString *startDate;
@property (nonatomic, copy) NSString *endDate;
@property (nonatomic, copy) NSString *checkIn;
@property (nonatomic, copy) NSString *checkOut;
@property (nonatomic, assign) long long checkInDate;
@property (nonatomic, assign) long long checkOutDate;
@property (nonatomic, assign) NSInteger days;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, assign) NSInteger type;
@property (nonatomic ,assign) long long timingTime;
@end

NS_ASSUME_NONNULL_END
