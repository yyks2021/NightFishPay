//
//  NightFishHotelModel.m
//  NightFish
//
//  Created by 魏琦 on 2019/3/4.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import "NightFishHotelModel.h"
#import <NSObject+YYModel.h>
#import <UIKit/UIKit.h>

#import <UIColor+Hex.h>
#import <InlineFunction.h>
#import <NFMacro.h>
@class NightFishHotelList,NightFishHotelInfo,NightFishRoomsInfo;
@implementation NightFishHotelModel
+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"list" : [NightFishSearchHotelModel class] };
}
@end

@implementation NightFishSearchHotelModel
+ (NSDictionary *)modelContainerPropertyGenericClass {

    return @{@"hotelInfoC2bHotelSearchRoomInfoVos" : [NightFishRoomInfo class],@"hotelInfoHotelSearchHotelInfoRoomInfoVos":[NightFishRoomInfo class],@"roomCalendarInfos":[NightFishRoomCalendarInfo class],@"roomCalendarVipInfos":[NightFishRoomCalendarInfo class],@"roomList":[NightFishVipRoom class],@"rooms":[NightFishRoomsInfo class]};
}

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"identifier" : @"id",@"desc":@"description"};
    //从 json 过来的key 可以是id，ID，book_id。例子中 key 为 id。
}


@end


@implementation NightFishVipRoom
+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"roomInfo" : [NightFishRoomInfo class]};
}
@end

@implementation NightFishRoomInfo
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"identifier" : @"id",@"desc":@"description"};
    //从 json 过来的key 可以是id，ID，book_id。例子中 key 为 id。
}

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"roomInfo" : [NightFishRoomsInfo class],@"roomCalendarInfo":[NightFishRoomCalendarInfo class] };
}

- (void)setRoomInfoJson:(NSString *)roomInfoJson {
    _roomInfoJson = roomInfoJson;
    
    NSArray *array = arrayWithJsonString(roomInfoJson);
    
    NSMutableArray *dataArray = @[].mutableCopy;
    if (array.count > 0) {
        for (NSDictionary *dic in array) {
            if (dic.count > 0) {
                NightFishRoomDevice *device = [NightFishRoomDevice yy_modelWithJSON:dic];
                [dataArray addObject:device];
            }
            
        }
    }
    
    _roomArray = dataArray.copy;
    
}


- (void)setDevicesJson:(NSString *)devicesJson {
    _devicesJson = devicesJson;
    NSArray *array = arrayWithJsonString(devicesJson);
    
    NSMutableArray *dataArray = @[].mutableCopy;
    if (array.count > 0) {
        for (NSDictionary *dic in array) {
            if (dic.count > 0) {
                NightFishRoomDevice *device = [NightFishRoomDevice yy_modelWithJSON:dic];
                [dataArray addObject:device];
            }
            
        }
    }
    
    _deviceArray = dataArray.copy;
}

@end

@implementation NightFishRoomsInfo
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"identifier" : @"id",@"desc":@"description"};
    //从 json 过来的key 可以是id，ID，book_id。例子中 key 为 id。
}

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"calendars" : [NightFishRoomCalendarInfo class] };
}


@end

@implementation NightFishHotelInfo

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"identifier" : @"id",@"desc":@"description"};
    //从 json 过来的key 可以是id，ID，book_id。例子中 key 为 id。
}

+ (NSDictionary *)modelContainerPropertyGenericClass {
    /*
     @property (nonatomic, strong) NSArray <NightFishRoomCalendarInfo *> *roomCalendarInfos;
     @property (nonatomic, strong) NSArray <NightFishRoomCalendarInfo *> *roomCalendarVipInfos;
     @property (nonatomic, strong) NightFishRoomsInfo *roomInfo;
     */
    return @{@"rooms" : [NightFishRoomInfo class]};
}

- (NSMutableAttributedString *)attrDesc {
    NSString *desc = self.desc;
    NSMutableAttributedString *attStr = [self attributedStringWithHTMLString:[self htmlEntityDecode:desc]];
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByClipping;
    paraStyle.lineSpacing = 1.0;
    paraStyle.firstLineHeadIndent = 20;
    [attStr addAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size:13],NSForegroundColorAttributeName:[UIColor colorWithHexString:@"666666"],NSParagraphStyleAttributeName:paraStyle,NSKernAttributeName:@1.0} range:NSMakeRange(0, attStr.length)];
    
    return attStr;
}

- (CGFloat)addressHeight {
    
     CGSize size = [self.address boundingRectWithSize:CGSizeMake(SCREEN_WIDTH - 75,MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:nil context:nil].size;
    return size.height;
}

- (CGFloat)descHeight {
    NSArray *array = getLinesArrayOfStringInLabel(self.attrDesc, [UIFont fontWithName:@"PingFangSC-Regular" size:AdapterSizeFromIphone6(13)],SCREEN_WIDTH - 50);
    return array.count * 20 + 5;
}


- (NSString *)htmlEntityDecode:(NSString *)string
{
    string = [string stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];
    string = [string stringByReplacingOccurrencesOfString:@"&apos;" withString:@"'"];
    string = [string stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
    string = [string stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
    string = [string stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"]; // Do this last so that, e.g. @"&amp;lt;" goes to @"&lt;" not @"<"
    
    return string;
}

//将HTML字符串转化为NSAttributedString富文本字符串
- (NSMutableAttributedString *)attributedStringWithHTMLString:(NSString *)htmlString
{
    NSDictionary *options = @{ NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType,
                               NSCharacterEncodingDocumentAttribute :@(NSUTF8StringEncoding)};
    
    NSData *data = [htmlString dataUsingEncoding:NSUTF8StringEncoding];
    
    return [[NSMutableAttributedString alloc] initWithData:data options:options documentAttributes:nil error:nil];
}

//去掉 HTML 字符串中的标签
- (NSString *)filterHTML:(NSString *)html
{
    NSScanner * scanner = [NSScanner scannerWithString:html];
    NSString * text = nil;
    while([scanner isAtEnd]==NO)
    {
        //找到标签的起始位置
        [scanner scanUpToString:@"<" intoString:nil];
        //找到标签的结束位置
        [scanner scanUpToString:@">" intoString:&text];
        //替换字符
        html = [html stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>",text] withString:@""];
    }
    return html;
}

@end

@implementation NightFishHomeHotelSearchModel
+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"list" : [NightFishHotelInfo class]};
}

@end


@implementation NightFishRoomCalendarInfo
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"identifier" : @"id"};
}





@end


@implementation NightFishRoomDevice

@end
