//
//  NightFishShareModel.h
//  NightFish
//
//  Created by 魏琦 on 2019/3/15.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NightFishShareModel : NSObject
@property(nonatomic, copy) NSString *title;
@property(nonatomic, copy) NSString *content;
@property(nonatomic, copy) NSString *url;

@end

NS_ASSUME_NONNULL_END
