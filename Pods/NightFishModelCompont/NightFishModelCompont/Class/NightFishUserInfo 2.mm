//
//  NightFishUserInfo.m
//  NightFish
//
//  Created by steven vicky on 2019/2/28.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import "NightFishUserInfo.h"
#import <YYModel/YYModel.h>
@implementation NightFishUserInfo
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"identifier" : @"id"};
    //从 json 过来的key 可以是id，ID，book_id。例子中 key 为 id。
}
/*
 @property (retain) NSString *gender;
 @property (retain) NSString *gmtCreate;
 @property (retain) NSString *gmtModified;
 @property int identifier;
 @property (retain) NSString *name;
 @property (retain) NSString *nickname;
 @property (retain) NSString *parentId;
 @property (retain) NSString *password;
 @property (retain) NSString *phone;
 @property (retain) NSString *status;
 @property int type;
 @property (retain) NSString *username;
 @property int valid;
 */
WCDB_IMPLEMENTATION(NightFishUserInfo);
WCDB_SYNTHESIZE(NightFishUserInfo, identifier);
WCDB_SYNTHESIZE(NightFishUserInfo, nickname);
WCDB_SYNTHESIZE(NightFishUserInfo, password);
WCDB_SYNTHESIZE(NightFishUserInfo, phone);
WCDB_SYNTHESIZE(NightFishUserInfo, status);
WCDB_SYNTHESIZE(NightFishUserInfo, vipStatus);
WCDB_SYNTHESIZE(NightFishUserInfo, valid);
WCDB_SYNTHESIZE(NightFishUserInfo, token);
WCDB_SYNTHESIZE(NightFishUserInfo, loginStatus);
WCDB_SYNTHESIZE(NightFishUserInfo, avatar);
@end
