//
//  NFOrderListModel.m
//  AFNetworking
//
//  Created by 魏琦 on 2019/3/8.
//

#import "NFOrderListModel.h"
#import <YYModel/YYModel.h>
@implementation NFOrderModel

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"list" : [NFOrderListModel class]};
}
@end

@implementation NFOrderListModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"identifier" : @"id"};
    //从 json 过来的key 可以是id，ID，book_id。例子中 key 为 id。
}



@end
