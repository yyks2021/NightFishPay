//
//  NightFishUserModel.h
//  NightFish
//
//  Created by steven vicky on 2019/2/28.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NightFishUserInfo.h"
NS_ASSUME_NONNULL_BEGIN

@interface NightFishUserModel : NSObject
@property (nonatomic ,strong) NightFishUserInfo *user_info;
@property (nonatomic, copy) NSString *token;
@end

NS_ASSUME_NONNULL_END
