//
//  NFOrderListModel.h
//  AFNetworking
//
//  Created by 魏琦 on 2019/3/8.
//

#import <Foundation/Foundation.h>


@class NFOrderListModel;
@interface NFOrderModel : NSObject
@property(nonatomic,copy) NSArray <NFOrderListModel*>* _Nonnull list;

@end
//修改
NS_ASSUME_NONNULL_BEGIN

@interface NFOrderListModel : NSObject
@property(nonatomic,assign) long identifier;
@property(nonatomic,copy) NSString * snapshotHotelName;
@property(nonatomic,copy) NSString * snapshotRoomName;
@property(nonatomic,assign) long snapshotOriginTotalFee;
@property(nonatomic,assign) long snapshotRefundFee;
@property(nonatomic,assign) long snapshotRoomPrice;
@property(nonatomic,assign) long snapshotTotalFee;
@property(nonatomic,assign) long status;
@property(nonatomic,assign) long long checkIn;
@property(nonatomic,assign) long long checkOut;
@property(nonatomic,assign) long long paymentMethod;
@property(nonatomic,copy) NSString *orderNo;
@property(nonatomic,assign) long roomId;
@property(nonatomic,assign) long days;
@property(nonatomic,copy) NSString *hotelPhone;
@property(nonatomic,assign) long long gmtCreate;
@property(nonatomic,copy) NSArray *roomImages;
@property(nonatomic,assign) NSInteger vipFlag; //0不是vip,1是vip
@property(nonatomic, copy) NSString *contactName;
@property(nonatomic, copy) NSString *contactPhone;
@property(nonatomic, copy) NSString *extraInfo;
@property(nonatomic, assign) long feeValue;
@property(nonatomic, assign) NSInteger hotelId;
@property(nonatomic, copy) NSArray*hotelImages;
@property(nonatomic, copy) NSString *leaveMessage;
@property(nonatomic, copy) NSString *hotelTel;
@property(nonatomic, copy) NSString *sourceType;
@property(nonatomic, assign) long sourceRoomId;
@property(nonatomic, assign) long sourceOrderId;
@property(nonatomic, assign) long sourceAmount;
@property(nonatomic, assign) long sourceHotelId;
//snapshotTotalFee = 60000;
//sourceAmount = 56200;
//sourceHotelId = 82205;
//sourceOrderId = "<null>";
//sourceRoomId = 227408;
//sourceType = cn;
@end

NS_ASSUME_NONNULL_END
