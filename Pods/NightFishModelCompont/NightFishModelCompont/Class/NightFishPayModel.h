//
//  NightFishPayModel.h
//  NightFish
//
//  Created by 魏琦 on 2019/3/7.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NightFishPayModel : NSObject
@property(nonatomic,copy) NSString *payInfo;
@property(nonatomic,copy) NSString *orderNo;
@end

NS_ASSUME_NONNULL_END
