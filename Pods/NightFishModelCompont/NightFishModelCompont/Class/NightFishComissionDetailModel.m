//
//  NightFishComissionDetailModel.m
//  NightFish
//
//  Created by steven vicky on 2019/4/28.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import "NightFishComissionDetailModel.h"
#import <NFMacro.h>
@implementation NightFishComissionDetailModel
- (CGFloat)height {
    CGFloat height = [self.content boundingRectWithSize:CGSizeMake(SCREEN_WIDTH - 90, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} context:nil].size.height;
    return height;
}
@end
