//
//  NightFishActiveConfigModel.h
//  NightFish
//
//  Created by steven vicky on 2019/5/6.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NightFishActiveConfigModel : NSObject
@property (nonatomic, copy) NSString *tuiUrl;
@property (nonatomic, copy) NSString *promoteUrl;
@property (nonatomic, copy) NSString *commissionUrl;
@property (nonatomic, copy) NSString *giftUrl;
@property (nonatomic, copy) NSString *activityUrl;

@end

NS_ASSUME_NONNULL_END
