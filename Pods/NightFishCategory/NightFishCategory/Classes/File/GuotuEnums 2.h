//
//  GuotuEnums.h
//  GuotuServer
//
//  Created by 魏琦 on 16/9/27.
//  Copyright © 2016年 hzdracom. All rights reserved.
//

#ifndef GuotuEnums_h
#define GuotuEnums_h
#pragma -mark 资源分类
typedef NS_ENUM(NSInteger,KResourceType) {
    KJournalType = 1,//期刊
    KNotJournalType//非期刊
};
#pragma -mark 首页
typedef NS_ENUM(NSInteger,KMsgType) {
    KNewsType = 1,//新闻
    KMaintenanceType,//维护
    KSpecialSubjectType,//专题
    KActivityType//活动
};
typedef NS_ENUM(NSInteger, KShowDisCoveryShow){
    KShowDisCovery = 1,//是
    KNotShowDisCovery//否 即首页传2,发现传1
};
typedef NS_ENUM(NSInteger,KType){
    KBookType = 1,//书籍
    KPDFType,//电子文稿
    KPicturesType,//图集
    KAudioType,//音频
    KVideoType//视屏
};

#pragma -mark 图书资源
typedef NS_ENUM(NSInteger,KResType){
    KLibraryResourceTypeNotJournal = 1,//(资源)非期刊
    KLibraryResourceTypeJournalType,//期刊
    KLibraryResourceTypeSpecialSubjectType//专题
};
#pragma -mark 个人中心
typedef NS_ENUM(NSInteger, KMessageType) {
    KMessageTypeOther = 0,//其他
    KMessageTypeSuggest,//建议
    KMessageTypeComplaints,//投诉
    KMessageTypeHelp,//求助
    KMessageTypeCorrection//纠错
};

typedef NS_ENUM(NSInteger, UserSex) {
    UserSexUnknown = 0,//未分配
    UserSexMale,//男
    UserSexFemale//女
};

typedef NS_ENUM(NSInteger, UserCardType) {
    UserCardTypeIdentifierCard = 0,//身份证
    UserCardTypeOfficer,//军官证
    UserCardTypePassport,//护照
    UserCardTypeHongKongAndMacaoPass,//港澳通行证
    UserCardTypeTaiwanPass//台湾通行证
    
};

/*
 //MP3的传输方式
 //    NSString* str = [[NSBundle mainBundle] pathForResource:@"宋秉洋 - 雨天" ofType:@"mp3"];
 //    NSData* data = [NSData dataWithContentsOfFile:str];
 //    NSLog(@"%@",data);
 
 //MP4上传
 //    NSString* str = [[NSBundle mainBundle] pathForResource:@"06-往网页中添加内容(掌握)" ofType:@"mp4"];
 //    NSData* data = [NSData dataWithContentsOfFile:str];
 //TXT格式上传
 //text/txt,text/plain
 
 //pdf上传
 //application/pdf
 
 //git上传
 //image/gif
 
 //ppt上传
 //application/vnd.ms-powerpoint
 
 //docx上传
 //application/vnd.openxmlformats-officedocument.wordprocessingml.document
 
 //xlsx上传
 //application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
 
 //doc文件上传
 //application/msword
 
 //xls上传
 //application/vnd.ms-excel
 
 //pptx上传
 //application/vnd.openxmlformats-officedocument.presentationml.presentation
 */

typedef NS_ENUM(NSInteger, KFileMineType){
    KFileMineTypeMp3 = 1,
    KFileMineTypeMp4,
    KFileMineTypeTXT,
    KFileMineTypePPT,
    KFileMineTypeDOC,
    KFileMineTypeDOCX,
    KFileMineTypeXLS,
    KFileMineTypeXLSX,
    KFileMineTypeJPG,
    KFileMineTypeJEPG,
    KFileMineTypeGIF,
    KFileMineTypePNG,
    KFileMineTypePDF,
    KFileMineTypePPTX
    
};

#endif /* GuotuEnums_h */
