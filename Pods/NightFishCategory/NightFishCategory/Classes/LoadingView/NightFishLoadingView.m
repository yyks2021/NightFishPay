//
//  NightFishLoadingView.m
//  NightFish
//
//  Created by 魏琦 on 2019/3/15.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import "NightFishLoadingView.h"
#import <SDWebImage/SDAnimatedImageView.h>
@interface NightFishLoadingView ()

@property (nonatomic,strong) SDAnimatedImageView *imageView;
@property (nonatomic, strong) UIView *backView;

@end

@implementation NightFishLoadingView



+ (void)showInView:(UIView *)view  imageWithImage:(UIImage *)image{
    NightFishLoadingView *instance = [[NightFishLoadingView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
   
    if (view == nil) {
        view = [UIApplication sharedApplication].keyWindow;
    }
    [view addSubview:instance];
    instance.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.35];
    instance.backView = [[UIView alloc] init];
    instance.backView.frame = CGRectMake(0, 0, image.size.width + 60, image.size.height + 60);
    [instance addSubview:instance.backView];
    instance.backView.layer.cornerRadius = 5;
    instance.backView.layer.masksToBounds = YES;

    
    
    UIBlurEffect *beffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
    UIVisualEffectView *effective = [[UIVisualEffectView alloc]initWithEffect:beffect];
    effective.alpha = 1.0;
    effective.frame = instance.backView.frame;
    [instance.backView addSubview:effective];
    
    CGFloat navigationHeight;
    if (@available(iOS 11.0, *)) {
        if ([[UIApplication sharedApplication].delegate window].safeAreaInsets.bottom > 0.0) {
            navigationHeight = 88;
        }
        else {
            navigationHeight = 64;
        }
    } else {
        navigationHeight = 64;
    }
    instance.backView.center =  CGPointMake([UIScreen mainScreen].bounds.size.width / 2, [UIScreen mainScreen].bounds.size.height / 2 - navigationHeight);
    
    instance.imageView = [[SDAnimatedImageView alloc] init];
    instance.imageView.frame  = CGRectMake((instance.backView.frame.size.width - image.size.width) / 2.0, (instance.backView.frame.size.height - image.size.height)/2.0, image.size.width, image.size.height);
    [instance.backView addSubview:instance.imageView];
    instance.imageView.image = image;
}

+ (void)dismissWithView:(UIView *)view {
    NSEnumerator *subviewsEnum = [view.subviews reverseObjectEnumerator];
    for (UIView *subview in subviewsEnum) {
        if ([subview isKindOfClass:self]) {
            NightFishLoadingView *hud = (NightFishLoadingView *)subview;
            if ([NSThread isMainThread]) {
                 [hud removeFromSuperview];
            }
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                     [hud removeFromSuperview];
                });
            }
           
        }
    }
}

- (UIImageView *)imageView {
    if (!_imageView) {
        
        SDAnimatedImageView *img = [[SDAnimatedImageView alloc]init];
        [self addSubview:img];
        _imageView = img;
    }
    return _imageView;
    
}



@end
