//
//  NightFishLoadingView.h
//  NightFish
//
//  Created by 魏琦 on 2019/3/15.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NightFishLoadingView : UIView
+ (void)showInView:(UIView *)view  imageWithImage:(UIImage *)image;
+ (void)dismissWithView:(UIView *)view;
@end

NS_ASSUME_NONNULL_END
