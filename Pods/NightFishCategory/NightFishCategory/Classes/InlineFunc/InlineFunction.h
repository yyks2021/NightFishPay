//
//  InlineFunction.h
//  NightFishCategory
//
//  Created by steven vicky on 2019/11/11.
//

#ifndef InlineFunction_h
#define InlineFunction_h
#import <WebKit/WebKit.h>
#import <CoreText/CoreText.h>
#import <UIKit/UIKit.h>
NS_INLINE NSString *getTommary(NSDate *aDate,NSInteger day,NSString *format) {
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorian components:NSCalendarUnitWeekday | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:aDate];
    [components setDay:([components day]+day)];
    
    NSDate *beginningOfWeek = [gregorian dateFromComponents:components];
    NSDateFormatter *dateday = [[NSDateFormatter alloc] init];
    [dateday setDateFormat:format];
    return [dateday stringFromDate:beginningOfWeek];
}


NS_INLINE long long dateTimestamp(NSString *dateStr) {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"YYYY年MM月dd日"];
    NSTimeZone* timeZone = [NSTimeZone systemTimeZone];
    
    NSDate *date = [formatter dateFromString:dateStr];
    NSInteger interval = [timeZone secondsFromGMTForDate:date];
    NSDate *localDate = [date dateByAddingTimeInterval:interval];
    
    long long time = [localDate timeIntervalSince1970] ;
    return time * 1000;
}

NS_INLINE BOOL isPureNum(NSString *text){
    if (!text) {
        return NO;
    }
    NSScanner *scan = [NSScanner scannerWithString:text];
    int val;
    return [scan scanInt:&val] && [scan isAtEnd];
}

NS_INLINE long long cTimestampFromString(NSString *theTime)  {
    NSDateFormatter*formatter=[[NSDateFormatter alloc]init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"YYYY年MM月dd日"];
    NSDate* dateTodo=[formatter dateFromString:theTime];
    NSTimeInterval timeSp = [dateTodo timeIntervalSince1970];
    return timeSp;
}

NS_INLINE BOOL nf_judgeNeedVersionUpdate(){
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSNumber *currentDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"currentDate"];
    long long now = [[NSDate date] timeIntervalSince1970];
    if (currentDate != nil) {
        if (now -  [currentDate longLongValue] <= 21600) {
            return NO;
        }
        else {
            [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithLongLong:now] forKey:@"currentDate"];
            return YES;
        }
    }
    else {
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithLongLong:now] forKey:@"currentDate"];
        return YES;
    }
}

NS_INLINE NSString * weekdayStringFromDate(NSString* inputDate) {
    //设置转换格式
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    [formatter setDateFormat:@"yyyy年MM月dd日"];
    //NSString转NSDate
    NSDate *date = [formatter dateFromString:inputDate];
    NSTimeZone *timeZone = [[NSTimeZone alloc] initWithName:@"Asia/Shanghai"];
    NSInteger interval = [timeZone secondsFromGMTForDate: date];
    NSDate *localeDate = [date dateByAddingTimeInterval: interval];
    NSArray *weekdays = [NSArray arrayWithObjects: [NSNull null],@"周日", @"周一", @"周二", @"周三", @"周四", @"周五", @"周六",  nil];
    
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierChinese];
    
    [calendar setTimeZone: timeZone];
    
    NSCalendarUnit calendarUnit = NSCalendarUnitWeekday;
    
    NSDateComponents *theComponents = [calendar components:calendarUnit fromDate:localeDate];
    
    return [weekdays objectAtIndex:theComponents.weekday];
    
}

NS_INLINE void changeUserAgent(WKWebView *webView) {
    [webView evaluateJavaScript:@"navigator.userAgent" completionHandler:^(id _Nullable result, NSError * _Nullable error) {
        NSDictionary *info = [NSBundle mainBundle].infoDictionary;
           NSString *newUserAgent = [result stringByAppendingString:[info objectForKey:@"CFBundleIdentifier"]];//自定义需要拼接的字符串
           NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:newUserAgent, @"UserAgent", nil];
           [[NSUserDefaults standardUserDefaults] registerDefaults:dictionary];
    }];
   
}

NS_INLINE NSURL * ma_AssistantLibraryBundleURLWithClass(Class cls,NSString *resources){
    NSBundle *bundle = [NSBundle bundleForClass:[cls class]];
    return [bundle URLForResource:resources withExtension:@"bundle"];
}

NS_INLINE NSBundle * ma_AssistantBundleWithClass(Class cls,NSString *resources) {
    //再获取我们自己手动创建的bundle
    return [NSBundle bundleWithURL:ma_AssistantLibraryBundleURLWithClass(cls,resources)];
}

NS_INLINE UIImage * ma_AssistantImageNamed(NSString *imageName,Class cls,NSString *resources)
{
    NSInteger scale = [UIScreen mainScreen].scale;
    UIImage *loadingImage = [[UIImage imageWithContentsOfFile:[ma_AssistantBundleWithClass(cls,resources) pathForResource:[NSString stringWithFormat:@"%@@%zdx",imageName,scale] ofType:@"png"]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    return loadingImage;
}

NS_INLINE long long getNowTimestamp() {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"]; //
    
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Beijing"];
    [formatter setTimeZone:timeZone];
    
    NSDate *datenow = [NSDate date];//现在时间
    
    //时间转时间戳的方法:
    long long timeSp = [datenow timeIntervalSince1970] *1000;
    
    return timeSp;
}
#define ARC4RANDOM_MAX      0x100000000
NS_INLINE double randomBetween() {
    double val = ((double)arc4random() / ARC4RANDOM_MAX);
    
    return val;
}


NS_INLINE NSDictionary * dicWithJsonString(NSString *jsonStr) {
    if(jsonStr ==nil) {
        
        return nil;
        
    }
    
    NSData*jsonData = [jsonStr dataUsingEncoding:NSUTF8StringEncoding];
    
    NSError*err;
    
    NSDictionary*dic = [NSJSONSerialization JSONObjectWithData:jsonData
                        
                                                       options:NSJSONReadingMutableContainers
                        
                                                         error:&err];
    
    if(err) {
        
        NSLog(@"json解析失败：%@",err);
        
        return nil;
    }
    
    NSMutableDictionary*newdict=[[NSMutableDictionary alloc]init];
    
    for(NSString*keys in dic)
        
    {
        
        if(dic[keys]==[NSNull null])
            
        {
            
            [newdict setObject:@"" forKey:keys];
            
            continue;
            
        }
        
        [newdict setObject:[NSString stringWithFormat:@"%@",dic[keys]]forKey:keys];
        
    }
    

    
    return newdict;
    
}


NS_INLINE NSArray * arrayWithJsonString(NSString *jsonStr) {
    if(jsonStr ==nil) {
        
        return nil;
        
    }
    
    while ([jsonStr containsString:@"\r\n"]) {

    jsonStr = [jsonStr stringByReplacingOccurrencesOfString:@"\r\n" withString:@""];

    }

    while ([jsonStr containsString:@"\t"]) {

    jsonStr = [jsonStr stringByReplacingOccurrencesOfString:@"\t" withString:@""];

    }

    while ([jsonStr containsString:@"\n"]) {

    jsonStr = [jsonStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];

    }
    
    NSData*jsonData = [jsonStr dataUsingEncoding:NSUTF8StringEncoding];
    
    NSError*err;
    
    NSArray*array = [NSJSONSerialization JSONObjectWithData:jsonData
                        
                                                       options:kNilOptions
                        
                                                         error:&err];
    
    if(err) {
        
        NSLog(@"json解析失败：%@",err);
        
        return nil;
    }
    return array;
    
}


NS_INLINE  UIImage * reSizeImageWithSize(UIImage *image,CGSize reSize)
{
UIGraphicsBeginImageContext(CGSizeMake(reSize.width, reSize.height));
[image drawInRect:CGRectMake(0, 0, reSize.width, reSize.height)];
UIImage *reSizeImage = UIGraphicsGetImageFromCurrentImageContext();
UIGraphicsEndImageContext();

return reSizeImage;

}

NS_INLINE NSArray *getLinesArrayOfStringInLabel(NSMutableAttributedString *attStr,UIFont *font,CGFloat lableWidth) {
    CTFramesetterRef frameSetter = CTFramesetterCreateWithAttributedString(( CFAttributedStringRef)attStr);
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddRect(path, NULL, CGRectMake(0,0,lableWidth,100000));
    CTFrameRef frame = CTFramesetterCreateFrame(frameSetter, CFRangeMake(0, 0), path, NULL);
    NSArray *lines = ( NSArray *)CTFrameGetLines(frame);
    NSMutableArray *linesArray = [[NSMutableArray alloc]init];
    for (id line in lines) {
        CTLineRef lineRef = (__bridge  CTLineRef )line;
        CFRange lineRange = CTLineGetStringRange(lineRef);
        NSRange range = NSMakeRange(lineRange.location, lineRange.length);
        NSString *lineString = [attStr.string substringWithRange:range];
        CFAttributedStringSetAttribute((CFMutableAttributedStringRef)attStr, lineRange, kCTKernAttributeName, (CFTypeRef)([NSNumber numberWithFloat:0.0]));
        CFAttributedStringSetAttribute((CFMutableAttributedStringRef)attStr, lineRange, kCTKernAttributeName, (CFTypeRef)([NSNumber numberWithInt:0.0]));
        [linesArray addObject:lineString];
    }
    
    CGPathRelease(path);
    CFRelease( frame );
    CFRelease(frameSetter);
    return (NSArray *)linesArray;
}




#endif /* InlineFunction_h */
