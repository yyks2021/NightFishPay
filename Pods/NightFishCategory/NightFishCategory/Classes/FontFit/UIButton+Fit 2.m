//
//  UIButton+Fit.m
//  NightFish
//
//  Created by steven vicky on 2019/10/17.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import "UIButton+Fit.h"
#import <objc/runtime.h>

@implementation UIButton (Fit)

static void *buttonFitFontSize_key = &buttonFitFontSize_key;
- (void)setFitFontSize:(CGFloat)fitFontSize {
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    
    self.titleLabel.font = [UIFont fontWithName:self.titleLabel.font.fontName size:width / 375.0 * fitFontSize];
    objc_setAssociatedObject(self, &buttonFitFontSize_key, @(fitFontSize), OBJC_ASSOCIATION_ASSIGN);
}

- (CGFloat)fitFontSize {
    return [objc_getAssociatedObject(self, &buttonFitFontSize_key) floatValue];
}
@end
