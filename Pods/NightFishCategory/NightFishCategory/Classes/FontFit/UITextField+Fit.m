//
//  UITextField+Fit.m
//  NightFish
//
//  Created by steven vicky on 2019/10/17.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import "UITextField+Fit.h"
#import <objc/runtime.h>
@implementation UITextField (Fit)
static void *textFieldFitFontSize_key = &textFieldFitFontSize_key;

- (void)setFitFontSize:(CGFloat)fitFontSize {
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    self.font = [UIFont fontWithName:self.font.fontName size:width / 375.0 * fitFontSize];
    objc_setAssociatedObject(self, &textFieldFitFontSize_key, @(fitFontSize), OBJC_ASSOCIATION_ASSIGN);
}

- (CGFloat)fitFontSize {
    return [objc_getAssociatedObject(self, &textFieldFitFontSize_key) floatValue];
}
@end
