//
//  UITextView+Fit.h
//  NightFish
//
//  Created by steven vicky on 2019/10/17.
//  Copyright © 2019 steven vicky. All rights reserved.
//
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITextView (Fit)
@property (nonatomic, assign)IBInspectable CGFloat fitFontSize;
@end

NS_ASSUME_NONNULL_END
