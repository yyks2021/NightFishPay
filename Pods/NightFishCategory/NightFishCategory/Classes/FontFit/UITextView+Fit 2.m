//
//  UITextView+Fit.m
//  NightFish
//
//  Created by steven vicky on 2019/10/17.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import "UITextView+Fit.h"
#import <objc/runtime.h>
@implementation UITextView (Fit)
static void *textViewFitFontSize_key = &textViewFitFontSize_key;

- (void)setFitFontSize:(CGFloat)fitFontSize {
    
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
       self.font = [UIFont fontWithName:self.font.fontName size:width / 375.0 * fitFontSize];
    objc_setAssociatedObject(self, &textViewFitFontSize_key, @(fitFontSize), OBJC_ASSOCIATION_ASSIGN);
}

- (CGFloat)fitFontSize {
    return [objc_getAssociatedObject(self, &textViewFitFontSize_key) floatValue];
}
@end
