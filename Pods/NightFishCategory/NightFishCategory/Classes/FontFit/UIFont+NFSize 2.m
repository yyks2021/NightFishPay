//
//  UIFont+NFSize.m
//  NightFish
//
//  Created by steven vicky on 2019/10/17.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import "UIFont+NFSize.h"
#import <objc/runtime.h>
@implementation UIFont (NFSize)
+(void)load {
    //获取替换后的类方法
       Method newMethod = class_getClassMethod([self class], @selector(adjustFont:));
       //获取替换前的类方法
       Method method = class_getClassMethod([self class], @selector(systemFontOfSize:));
       //然后交换类方法
       method_exchangeImplementations(newMethod, method);

       Method newMethod1 = class_getClassMethod([self class], @selector(boldAdjustFont:));
    //获取替换前的类方法
        Method method1 = class_getClassMethod([self class], @selector(boldSystemFontOfSize:));
    //然后交换类方法
        method_exchangeImplementations(newMethod1, method1);
}

+(UIFont *)adjustFont:(CGFloat)fontSize{

    if (fontSize >= 44) {
        UIFont *newFont = nil;
        newFont = [UIFont adjustFont:fontSize];
        return newFont;
    }
    else {
        UIFont *newFont = nil;
         CGFloat width = [UIScreen mainScreen].bounds.size.width;
        newFont = [UIFont adjustFont:( width / 375.0 * fontSize)];
        return newFont;
    }
    
}

+(UIFont *)boldAdjustFont:(CGFloat)fontSize{

    if (fontSize >= 44) {
        UIFont *newFont = nil;
        newFont = [UIFont boldAdjustFont: fontSize];
        return newFont;
    }
    else {
        UIFont *newFont = nil;
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        newFont = [UIFont boldAdjustFont: (width / 375.0 * fontSize)];
        return newFont;
    }
    
}


@end
