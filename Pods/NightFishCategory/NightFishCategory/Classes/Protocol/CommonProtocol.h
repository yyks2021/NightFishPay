//
//  CommonProtocol.h
//  NightFishCategory
//
//  Created by steven vicky on 2019/11/14.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol CommonProtocol <NSObject>
- (void)cityName:(NSString *)name;

@end

NS_ASSUME_NONNULL_END
