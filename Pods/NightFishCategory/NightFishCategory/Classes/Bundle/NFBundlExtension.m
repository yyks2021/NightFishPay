//
//  NFBundlExtension.m
//  NightFishCategory
//
//  Created by 魏琦 on 2019/3/27.
//

#import "NFBundlExtension.h"

@implementation NFBundlExtension
- (NSURL *)ma_AssistantLibraryBundleURLWithClass:(Class)cls {
    NSBundle *bundle = [NSBundle bundleForClass:[cls class]];
    return [bundle URLForResource:@"source" withExtension:@"bundle"];
}

- (NSBundle *) ma_AssistantBundleWithClass:(Class)cls {
    //再获取我们自己手动创建的bundle
    return [NSBundle bundleWithURL:[self ma_AssistantLibraryBundleURLWithClass:cls]];
}

- (UIImage *)ma_AssistantImageNamed:(NSString *)imageName Class:(Class)cls
{
    NSInteger scale = [UIScreen mainScreen].scale;
    UIImage *loadingImage = [[UIImage imageWithContentsOfFile:[[self ma_AssistantBundleWithClass:cls] pathForResource:[NSString stringWithFormat:@"%@@%zdx",imageName,scale] ofType:@"png"]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    return loadingImage;
}

@end
