//
//  NFBundlExtension.h
//  NightFishCategory
//
//  Created by 魏琦 on 2019/3/27.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NFBundlExtension : NSObject
- (UIImage *)ma_AssistantImageNamed:(NSString *)imageName Class:(Class)cls;
@end

NS_ASSUME_NONNULL_END
