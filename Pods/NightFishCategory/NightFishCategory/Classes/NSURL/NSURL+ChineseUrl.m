//
//  NSURL+ChineseUrl.m
//  NightFish
//
//  Created by 魏琦 on 2019/3/21.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import "NSURL+ChineseUrl.h"
#import <objc/runtime.h>
@implementation NSURL (ChineseUrl)
+ (void)load {
    Method URLWithStringMethod = class_getClassMethod(self, @selector(URLWithString:));
    
    // 获取sc_imageWithName:方法的地址
    Method sc_URLWithStringMethod = class_getClassMethod(self, @selector(sc_URLWithString:));
    
    // 交换方法地址，相当于交换实现方式2
    method_exchangeImplementations(URLWithStringMethod, sc_URLWithStringMethod);
    
}


+ (NSURL *)sc_URLWithString:(NSString *)URLString {
    
    NSString *newURLString = [self IsChinese:URLString];
    return [NSURL sc_URLWithString:newURLString];
}

//判断是否有中文
+ (NSString *)IsChinese:(NSString *)str {
    NSString *newString = str;
    
    for(int i=0; i< [str length];i++){
        int a = [str characterAtIndex:i];
        if( a > 0x4e00 && a < 0x9fff)
        {
            NSString *oldString = [str substringWithRange:NSMakeRange(i, 1)];
            NSString *string = [oldString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            newString = [newString stringByReplacingOccurrencesOfString:oldString withString:string];
        } else{
            
        }
    }
    return newString;
}
@end
