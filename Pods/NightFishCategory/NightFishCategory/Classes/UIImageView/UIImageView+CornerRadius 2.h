//
//  UIImageView+CornerRadius.h
//  NightFish
//
//  Created by steven vicky on 2019/10/23.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import <UIKit/UIKit.h>




NS_ASSUME_NONNULL_BEGIN

@interface UIImageView (CornerRadius)
- (void)quickSetCornerRadius:(CGFloat)cornerRadius cornerDirection:(UIRectCorner)cornerDirection;
@end

NS_ASSUME_NONNULL_END
