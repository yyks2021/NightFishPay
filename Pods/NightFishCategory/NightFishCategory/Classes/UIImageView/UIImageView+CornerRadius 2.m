//
//  UIImageView+CornerRadius.m
//  NightFish
//
//  Created by steven vicky on 2019/10/23.
//  Copyright © 2019 steven vicky. All rights reserved.
//

#import "UIImageView+CornerRadius.h"

@interface UIImage (CornerRadius)
- (UIImage *)imageAddCornerWithRadius:(CGFloat)radius andSize:(CGSize)size cornerDirection:(UIRectCorner)cornerDirection;

@end

@implementation UIImageView (CornerRadius)
- (void)quickSetCornerRadius:(CGFloat)cornerRadius
{
    [self quickSetCornerRadius:cornerRadius cornerDirection:UIRectCornerAllCorners];
}


- (void)quickSetCornerRadius:(CGFloat)cornerRadius cornerDirection:(UIRectCorner)cornerDirection {
    self.image = [self.image imageAddCornerWithRadius:cornerRadius andSize:self.bounds.size cornerDirection:cornerDirection];
}

@end


@implementation UIImage (cornerRadius)

- (UIImage *)imageAddCornerWithRadius:(CGFloat)radius andSize:(CGSize)size cornerDirection:(UIRectCorner)cornerDirection
{
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    
    UIGraphicsBeginImageContextWithOptions(size, NO, [UIScreen mainScreen].scale);
    CGContextRef contextRef = UIGraphicsGetCurrentContext();
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:rect byRoundingCorners:cornerDirection cornerRadii:CGSizeMake(radius, radius)];
    CGContextAddPath(contextRef,path.CGPath);
    CGContextClip(contextRef);
    [self drawInRect:rect];
    CGContextDrawPath(contextRef, kCGPathFillStroke);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

@end
