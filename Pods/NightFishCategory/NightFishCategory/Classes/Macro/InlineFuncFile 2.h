//
//  InlineFuncFile.h
//  NightFishCategory
//
//  Created by 魏琦 on 2019/3/27.
//

#ifndef InlineFuncFile_h
#define InlineFuncFile_h
static inline BOOL isMobilePhone(NSString *phone) {
    NSString * MOBILE = @"^(1)\\d{10}$";
    
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    
    if ([regextestmobile evaluateWithObject:phone] == YES)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    //^[a-zA-Z]\w{5,17}$
}

static inline BOOL passwordValiad(NSString *password) {
    NSString * MOBILE = @"^[a-zA-Z][a-zA-Z0-9_]*$";
    
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    
    if ([regextestmobile evaluateWithObject:password] == YES)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    //^[a-zA-Z]\w{5,17}$
}

static inline BOOL  passwordLength(NSString *password) {
    NSString * MOBILE = @"^[a-zA-Z][a-zA-Z0-9_]{5,15}$";
    
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    
    if ([regextestmobile evaluateWithObject:password] == YES)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

static inline NSDictionary* dictionaryWithJsonString(NSString *jsonString) {
    if (jsonString == nil) {
        return nil;
    }
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    if(err) {
        NSLog(@"json解析失败：%@",err);
        return nil;
    }
    return dic;
}

NS_INLINE CGFloat navigationBarHeight() {
    CGFloat navigationHeight;
    if (@available(iOS 11.0, *)) {
        if ([[UIApplication sharedApplication].delegate window].safeAreaInsets.bottom > 0.0) {
            navigationHeight = 88;
        }
        else {
            navigationHeight = 64;
        }
    } else {
        navigationHeight = 64;
    }
    return navigationHeight;
}

NS_INLINE BOOL isNum(NSString *checkedNumString){
    checkedNumString = [checkedNumString stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
    if(checkedNumString.length > 0) {
        return NO;
    }
    return YES;
}


#endif /* InlineFuncFile_h */
