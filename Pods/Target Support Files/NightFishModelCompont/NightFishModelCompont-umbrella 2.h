#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "JFAreaModel.h"
#import "NFOrderDetailModel.h"
#import "NFOrderListModel.h"
#import "NightFishAccountInfoModel.h"
#import "NightFishActiveConfigModel.h"
#import "NightFishBallInfoModel.h"
#import "NightFishCardAdvantageInfo.h"
#import "NightFishComissionDetailModel.h"
#import "NightFishComissionModel.h"
#import "NightFishCommonActivityShareModel.h"
#import "NightFishCouponRecomendModel.h"
#import "NightFishGenPlanModel.h"
#import "NightFishGoodListModel.h"
#import "NightFishGrabOrderModel.h"
#import "NightFishHavePassword.h"
#import "NightFishHelpModel.h"
#import "NightFishHotelModel.h"
#import "NightFishInvitedModel.h"
#import "NightFishMessageModel.h"
#import "NightFishOrderDetailModel.h"
#import "NightFishPayModel.h"
#import "NightFishRechargeListInfo.h"
#import "NightFishShareModel.h"
#import "NightFishSignInfoModel.h"
#import "NightFishUpdateVersionDetailModel.h"
#import "NightFishUpdateVersionModel.h"
#import "NightFishUserBindCardModel.h"
#import "NightFishUserInfo.h"
#import "NightFishUserMemberCardInfo.h"
#import "NightFishUserModel.h"
#import "NightFishWalltBalanceInfo.h"
#import "WCDBManager.h"

FOUNDATION_EXPORT double NightFishModelCompontVersionNumber;
FOUNDATION_EXPORT const unsigned char NightFishModelCompontVersionString[];

