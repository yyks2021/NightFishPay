# NightFishPay

[![CI Status](https://img.shields.io/travis/Vickeywei/NightFishPay.svg?style=flat)](https://travis-ci.org/Vickeywei/NightFishPay)
[![Version](https://img.shields.io/cocoapods/v/NightFishPay.svg?style=flat)](https://cocoapods.org/pods/NightFishPay)
[![License](https://img.shields.io/cocoapods/l/NightFishPay.svg?style=flat)](https://cocoapods.org/pods/NightFishPay)
[![Platform](https://img.shields.io/cocoapods/p/NightFishPay.svg?style=flat)](https://cocoapods.org/pods/NightFishPay)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

NightFishPay is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'NightFishPay'
```

## Author

Vickeywei, 17682309322@163.com

## License

NightFishPay is available under the MIT license. See the LICENSE file for more info.
